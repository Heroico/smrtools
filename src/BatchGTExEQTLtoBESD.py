#! /usr/bin/env python

import os
import logging

from gene_tools import Logging
from gene_tools import Utilities
from gene_tools.jobs import PBS
from gene_tools.smr import SMRExecution

SIGNIF = ".v6p.signif_snpgene_pairs.txt.gz"

def build_gtex_to_esd_command(smrtools_path, gtex_eqtl_path, gtex_frequency_path, gtex_snp_path, gencode_path, working_path, verbosity=10):
    command = "python {} \\\n".format(os.path.join(smrtools_path, "GTExToSMR.py"))
    command += "--gtex_eqtl_path {} \\\n".format(gtex_eqtl_path)
    command += "--gtex_frequency_path {} \\\n".format(gtex_frequency_path)
    command += "--gtex_snp_path {} \\\n".format(gtex_snp_path)
    command += "--gencode_path {} \\\n".format(gencode_path)
    command += "--working_path {} \\\n".format(working_path)
    command += "--verbosity {}".format(verbosity)
    return command

def candidate_path(folder, name):
    path = [x for x in os.listdir(folder) if name in x]
    path = os.path.join(folder, path)
    return path

def run(args):
    if not os.path.exists(args.jobs_folder): os.makedirs(args.jobs_folder)
    if not os.path.exists(args.logs_folder): os.makedirs(args.logs_folder)
    if not os.path.exists(args.working_path): os.makedirs(args.working_path)
    if not os.path.exists(args.results_path): os.makedirs(args.results_path)

    eqtls = [x for x in os.listdir(args.gtex_eqtl_folder) if SIGNIF in x]

    for i,eqtl in enumerate(eqtls):
        eqtl_name = eqtl.split(SIGNIF)[0]
        job_name = "job_geb_"+eqtl_name
        if "_Analysis" in job_name: job_name = job_name.split("_Analysis")[0]

        eqtl_path = os.path.join(args.gtex_eqtl_folder, eqtl)
        frequency_path = Utilities.candidate_path(args.gtex_frequency_folder, eqtl_name)
        working_path = os.path.join(args.working_path, eqtl_name)

        if os.path.exists(working_path):
            logging.info("%s already exists,remove it if you want it done again", working_path)
            continue

        command = PBS.job_header(job_name, module_clause="module load python/2.7.9", log_folder=args.logs_folder)
        command += build_gtex_to_esd_command(args.smrtools_path,
                                          eqtl_path, frequency_path,
                                          args.gtex_snp_path, args.gencode_path, working_path)
        command += "\n"

        flist = os.path.join(working_path, eqtl_name+".flist")
        besd_output = os.path.join(args.results_path, eqtl_name, eqtl_name)
        Utilities.ensure_requisite_folders(besd_output)
        command += SMRExecution.smr_besd(flist, besd_output, args.smr_command) + "\n\n"

        command += "rm -rf {} \n".format(working_path)
        job_id = PBS.submit_command(args.jobs_folder, job_name, command)
        msg = "Submission {}".format(job_id if job_id else "error for "+eqtl_name)
        logging.info(msg)

    logging.info("Batched!")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Convert GTEx eQTL to ESD")
    parser.add_argument("--jobs_folder",
                        help="folder where submission jobs will be saved",
                        default = "jobs")
    parser.add_argument("--logs_folder",
                        help="Folder where job logs will be saved",
                        default = "logs")
    parser.add_argument("--smrtools_path", help="Path to smr tools")
    parser.add_argument("--smr_command", help="what smr executable to use")
    parser.add_argument("--gtex_eqtl_folder", help="Path to GTEX eQTL studies (as downloaded form portal")
    parser.add_argument("--gtex_frequency_folder", help="Path to calculated frequencies (from GTEx genotypes)")
    parser.add_argument("--gtex_snp_path", help="Path to GTEx SNP annotation")
    parser.add_argument("--gencode_path", help="Gencode file matching the GTEx eQTL")
    parser.add_argument("--working_path", help="Where to svae intermediate stuff")
    parser.add_argument("--results_path", help="Where to save results")
    parser.add_argument("--verbosity", help="Logging verbosity", default=10)

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)