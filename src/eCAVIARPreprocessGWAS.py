#! /usr/bin/env python

import os
import logging

from gene_tools import Logging
from gene_tools.gwas import GWAS
from gene_tools.gwas import Utilities as GWASUtilities
from gene_tools.ecaviar import ECAVIAR
from gene_tools import Utilities


def run(args):
    if os.path.exists(args.output_path):
        logging.info("%s already exists. Delete it or move it if you want it done again", args.output_path)
        return

    logging.info("Loading gwas")
    gwas_format = GWASUtilities.gwas_format_from_args(args)
    GWAS.validate_format_basic(gwas_format)
    GWAS.validate_format_for_strict(gwas_format)

    #Just have the custom parser address all the input kinks we have to deal with.
    load_from = os.path.join(args.gwas_file_path)
    load_from = GWASUtilities.gwas_filtered_source(load_from, skip_until_header=args.skip_until_header)
    b = GWAS.load_gwas(load_from, gwas_format, sep="\s+")
    b = ECAVIAR.convert_gwas(b)
    Utilities.ensure_requisite_folders(args.output_path)
    compression = "gzip" if ".gz" in args.output_path else None
    b.to_csv(args.output_path,index=False,sep="\t", compression=compression)
    logging.info("Successfull conversion")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Convert GWAS to eCAVIAR preprocess")

    parser.add_argument("--gwas_file_path", help="Gwas input file path")
    parser.add_argument("--output_path", help="result file")
    parser.add_argument("--verbosity", help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything", default = "10")

    GWASUtilities.add_gwas_arguments_to_parser(parser)

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)