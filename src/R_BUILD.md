Or maybe one at a time
```
export CPPFLAGS='-I/home/abarbeira3/software/zlib-1.2.11/ -I/home/abarbeira3/software/bzip2-1.0.6 -I/home/abarbeira3/software/xz-5.2.3/src/liblzma/lzma -I/home/abarbeira3/software/pcre_build/include'
export LDFLAGS='-L/home/abarbeira3/software/zlib-1.2.11/ -L/home/abarbeira3/software/bzip2-1.0.6 -L/home/abarbeira3/software/xz-5.2.3/src/liblzma/.libs/ -L/home/abarbeira3/software/pcre_build/lib'
./configure --enable-R-shlib --with-blas --with-lapack
```