#! /usr/bin/env python

import os
import logging
import pandas
import numpy

import rpy2.robjects as robjects

from gene_tools import Logging
from gene_tools import Utilities
from rpy2.robjects.packages import importr
colocr = importr('coloc')

from gene_tools.misc.Wrangling import align_data_to_alleles

def helper(aligned):
    data = {}
    for i, row in enumerate(aligned.itertuples()):
        gene_id = row.gene_id
        if not gene_id in data:
            data[gene_id] = {"g_frequency":[], "g_beta":[], "g_se":[], "frequency":[], "beta":[], "se":[]}

        d=data[gene_id]
        d["g_frequency"].append(row.g_frequency)
        d["g_beta"].append(row.g_beta)
        d["g_se"].append(row.g_se)
        d["frequency"].append(row.frequency)
        d["beta"].append(row.beta)
        d["se"].append(row.se)
    return data

def _coloc(d, g_N, e_N):
    try:
        g_v = robjects.FloatVector(numpy.array(d["g_se"]) ** 2)
        g_f = robjects.FloatVector(d["g_frequency"])
        g_b = robjects.FloatVector(d["g_beta"])
        d1 = robjects.ListVector({"beta": g_b, "varbeta": g_v, "MAF": g_f, "N": g_N, "type": "quant"})

        v = robjects.FloatVector(numpy.array(d["se"]) ** 2)
        f = robjects.FloatVector(d["frequency"])
        b = robjects.FloatVector(d["beta"])
        d2 = robjects.ListVector({"beta": b, "varbeta": v, "MAF": f, "N": e_N, "type": "quant"})

        c = colocr.coloc_abf(dataset1=d1, dataset2=d2)
        r = c.rx('summary')[0]
        r = tuple(r[1:])
    except:
        r = (None,None,None,None,None)
        pass
    return r

def process(data, N1, N2):
    results = []
    for k, d in data.iteritems():
        r = _coloc(d, N1, N2)
        r = (k,)+r
        results.append(r)

    results = zip(*results) if len(results) else [[], [], [], [], [], []]

    columns = ["gene_id", "P_H0", "P_H1", "P_H2", "P_H3", "P_H4"]
    results = {columns[i]:results[i] for i in xrange(len(columns))}
    results = pandas.DataFrame(results)
    results = results[["gene_id", "P_H0", "P_H1", "P_H2", "P_H3", "P_H4"]]
    results = results.fillna("NA")
    return results

def run(args):
    if not args.eqtl_N:
        logging.info("Must provide sample size for eqtl")
        return

    if not args.gwas_N:
        logging.info("Must provide sample size for GWAs")
        return

    if os.path.exists(args.output):
        logging.info("%s already exists. Delete it or move it if you want it done again", args.output)
        return

    logging.info("Loading gwas")
    gwas = pandas.read_table(args.gwas_path)
    gwas = gwas.rename(columns={"A1":"effect_allele", "A2":"non_effect_allele", "b":"g_beta", "se":"g_se", "SNP":"rsid", "freq":"g_frequency"})

    logging.info("Loading eqtl")
    eqtl = pandas.read_table(args.eqtl_path)

    aligned = align_data_to_alleles(gwas, eqtl, left_on="rsid", right_on="rsid", flips=["g_beta"])
    aligned = aligned[["gene_id","rsid", "effect_allele", "non_effect_allele", "g_frequency", "g_beta", "g_se", "frequency", "beta", "se"]]
    aligned = aligned.dropna()

    logging.info("Converting")
    data = helper(aligned)

    logging.info("Running coloc")
    results = process(data, int(args.gwas_N), int(args.eqtl_N))

    Utilities.ensure_requisite_folders(args.output)
    results.to_csv(args.output, index=False, sep="\t")
    # aligned.to_csv(args.output, index=False, sep="\t", compression="gzip")
    logging.info("successfully ran coloc")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Run COLOC analysis between GWAS and GTEx eQTL")

    parser.add_argument("--eqtl_path",
                        help="path to eqtl file",
                        default="scripts/results/eqtl/Thyroid.txt.gz")

    parser.add_argument("--eqtl_N",
                        help="Number of samples in eQTL")

    parser.add_argument("--gwas_path",
                        help="path to GWAS file",
                        default="scripts/results/gwas/GIANT_HEIGHT.ma")

    parser.add_argument("--gwas_N",
                        help="Number of samples in GWAS")

    parser.add_argument("--output",
                        help="path where COLOC result will be built",
                        default="results/coloc/GIANT_HEIGHT_Thyroid.txt.gz")

    parser.add_argument("--verbosity",
                        help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything",
                        default = "10")

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)