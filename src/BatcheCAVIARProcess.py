#! /usr/bin/env python

import os
import logging

from gene_tools import Logging
from gene_tools.jobs import PBS

def build_command(args, gwas_file, eqtl_file):
    gwas = gwas_file.split(".txt.gz")[0]
    eqtl = eqtl_file.split(".txt.gz")[0]
    name = "_".join([gwas, "eQTL", eqtl])
    job_name = "_".join(["job_ecaviar",name])

    intermediate = os.path.join(args.working_folder, name)
    output = os.path.join(args.output_folder, name) + ".txt.gz"

    eqtl_reference = [x for x in os.listdir(args.eqtl_reference_folder) if eqtl in x][0]
    eqtl_reference = os.path.join(args.eqtl_reference_folder, eqtl_reference)

    command = PBS.job_header(job_name, module_clause="module load python/2.7.9", log_folder=args.logs_folder,
                             walltime="48:00:00", job_mem="16gb", email="abarbeira3@medicine.bsd.uchicago.edu")
    command += "python {} \\\n".format(args.script_path)
    command += "--eqtl_path {} \\\n".format(os.path.join(args.eqtl_folder, eqtl_file))
    command += "--eqtl_reference_file {} \\\n".format(eqtl_reference)
    command += "--gwas_path {} \\\n".format(os.path.join(args.gwas_folder, gwas_file))
    command += "--gwas_reference_folder {} \\\n".format(args.gwas_reference_folder)
    command += "--gwas_reference_pattern \"{}\" \\\n".format(args.gwas_reference_pattern)
    command += "--ecaviar_path {} \\\n".format(args.ecaviar_path)
    command += "--verbosity {} \\\n".format(args.verbosity)
    command += "--intermediate {} \\\n".format(intermediate)
    command +=  "--output {}".format(output)
    command += " > /dev/null"
    return job_name, command

def run(args):
    if not os.path.exists(args.jobs_folder): os.makedirs(args.jobs_folder)
    if not os.path.exists(args.logs_folder): os.makedirs(args.logs_folder)
    if not os.path.exists(args.working_folder): os.makedirs(args.working_folder)

    gwas_files = os.listdir(args.gwas_folder)
    eqtl_files = os.listdir(args.eqtl_folder)
    for gwas_file in gwas_files:
        for eqtl_file in eqtl_files:
            job_name, command = build_command(args, gwas_file, eqtl_file)
            job_id = PBS.submit_command(args.jobs_folder, job_name, command, fake=args.fake_submission ,serialize_local=args.serialize_local)
            msg = "Submitted {}|{}".format(str(job_id) if job_id else "error for %s" % (job_name), job_name)
            logging.info(msg)
    logging.info("Submitted.")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Convert GTEx eQTL to Preprocess format",
                                     epilog=
"""Watch out for the intermediate folder.
Will be used with extreme prejudice.""")
    parser.add_argument("--jobs_folder", help="Where the submission jobs will be saved", default="jobs")
    parser.add_argument("--logs_folder", help="Where the job logs will be saved", default="logs")
    parser.add_argument("--working_folder", help="working path with scratch data", default="_working")
    parser.add_argument("--output_folder", help="where to save results")
    parser.add_argument("--script_path", help="Path to script executable", default=None)
    parser.add_argument("--ecaviar_path", help="Path to eCAVIAR executable", default=None)
    parser.add_argument("--eqtl_folder", help="path to eqtl folder")
    parser.add_argument("--eqtl_reference_folder", help="Folder with eQTL genotypes", default=None)
    parser.add_argument("--gwas_folder", help="path to GWAS")
    parser.add_argument("--gwas_reference_folder", help="Path to reference panel data (PrediXcan format)",
        default="data/TGF_EUR_CAVIAR")
    parser.add_argument("--gwas_reference_pattern", help="Pattern to select dosage file (and extract chromosome)",
        default="1000GP_Phase3_chr([0-9]+).dosage.gz")
    parser.add_argument("--fake_submission", help="wether to submit or not", action="store_true", default=False)
    parser.add_argument("--serialize_local", help="Just run locally", action="store_true", default=False)
    parser.add_argument("--verbosity",
                        help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything",
                        default = "10")

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)