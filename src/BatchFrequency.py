#! /usr/bin/env python
import logging
import os
from gene_tools.jobs import PBS

from gene_tools import Logging

SIGNIF=".v6p.signif_snpgene_pairs.txt.gz"

def build_command(eqtl, eqtl_file, geno_file, output_file):
    job_name = "job_f_%s" % (eqtl)
    job = PBS.job_header(job_name, module_clause="module load python/2.7.9")
    job += "python /group/im-lab/nas40t2/abarbeira/software/smrtools/src/GTExSNPFrequency.py \\\n"
    job += "--gtex_eqtl_path %s \\\n" % (eqtl_file)
    job += "--gtex_geno_path %s \\\n" % (geno_file)
    job +=  "--output_path %s" % (output_file)
    return job_name, job

def loop_jobs(eqtl_folder, geno_folder, output_folder):
    eqtls = sorted([x for x in os.listdir(eqtl_folder) if SIGNIF in x])
    for i,eqtl in enumerate(eqtls):
        eqtl_file = os.path.join(eqtl_folder, eqtl)
        eqtl_name = eqtl.split(SIGNIF)[0]
        logging.info(eqtl_name)

        geno_file = [x for x in os.listdir(geno_folder) if eqtl_name in x][0]
        geno_file = os.path.join(geno_folder,geno_file)

        results_file = os.path.join(output_folder, eqtl_name) + ".txt"

        job_name, command = build_command(eqtl, eqtl_file, geno_file, results_file)
        job_id = PBS.submit_command("jobs", job_name, command)

        msg = "Submitted {}|{}".format(str(job_id) if job_id else "error for %s"%(eqtl_name), job_name)
        logging.info(msg)

    logging.info("Batched!")

def run(args):
    if not os.path.exists("jobs"): os.makedirs("jobs")
    if not os.path.exists("logs"): os.makedirs("logs")
    loop_jobs(args.gtex_eqtl_path, args.gtex_geno_path, args.output_path)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Gather frequencies from GTEx genos")


    parser.add_argument("--gtex_eqtl_path",
                        help="path to GTEx eqtl file",
                        default="data/eqtl")

    parser.add_argument("--gtex_geno_path",
                        help="path to GTEx snp file",
                        default="data/geno")

    parser.add_argument("--output_path",
                        help="path to Gencode file",
                        default="results")

    parser.add_argument("--verbosity",
                        help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything",
                        default = "10")

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)