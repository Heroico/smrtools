#! /usr/bin/env python

import os
import logging

from gene_tools import Logging
from gene_tools.smr import GTExEQTLtoESD

def run(args):
    if os.path.exists(args.working_path):
        logging.info("%s already exists. Delete it or move it if you want it done again", args.working_path)
        return

    GTExEQTLtoESD.build_esd_files(args.working_path, args.gtex_eqtl_path, args.gtex_snp_path, args.gtex_frequency_path, args.gencode_path)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Convert GTEx eQTL to ESD")

    parser.add_argument("--gtex_eqtl_path",
                        help="path to GTEx eqtl file",
                        default="data/GTEx_Analysis_v6p_eQTL/Adipose_Subcutaneous_Analysis.v6p.signif_snpgene_pairs.txt.gz")

    parser.add_argument("--gtex_frequency_path",
                        help="path to gtex frequency",
                        default="data/gtex_frequency/Adipose_Subcutaneous_Analysis.txt")

    parser.add_argument("--gtex_snp_path",
                        help="path to GTEx snp file",
                        default="data/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz")

    parser.add_argument("--gencode_path",
                        help="path to Gencode file",
                        default="data/gencode.v19.genes.v6p_model.patched_contigs.gtf.gz")

    parser.add_argument("--working_path",
                        help="path where SMR ESD will be built",
                        default="working/Adipose_Subcutaneous")

    parser.add_argument("--verbosity",
                        help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything",
                        default = "10")

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)