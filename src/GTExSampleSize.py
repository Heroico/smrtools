#! /usr/bin/env python
import os
import gzip
import pandas
PATH="/scratch/abarbeira3/data/GTEx/v6p/geno"

files = sorted(os.listdir(PATH))
paths = [os.path.join(PATH,x) for x in files]

results = []
for i in xrange(len(paths)):
    file_name = files[i].split(".snps")[0]
    path = paths[i]
    with gzip.open(path) as file:
        header = file.readline()
        count = len(header.split()) -1 #header has a first column called "ID", the rest are individuals ods
    results.append((file_name, count))

results = zip(*results)
columns = ["file_name","N"]
results = {columns[i]:results[i] for i in xrange(len(columns))}
results = pandas.DataFrame(results)
results = results[columns]

results.to_csv("GTEx_samples.txt", index=False, sep="\t")
