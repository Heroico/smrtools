__author__ = 'heroico'
#trimmed from PredictDBAnalysis/gencode_input

import pandas
from .. import Utilities

K_NOT_GENES = {"transcript","exon","CDS","UTR","start_codon","stop_codon","Selenocysteine"};

# look at gencode http://www.gencodegenes.org/data_format.html
class GFTF:
    """gencode file table format"""
    CHROMOSOME = 0
    ANNOTATION_SOURCE = 1
    FEATURE_TYPE = 2
    N_START_LOCATION = 3
    N_END_LOCATION = 4
    SCORE = 5
    GENOMIC_STRAND = 6
    GENOMIC_PHASE = 7
    KEY_VALUE_PAIRS = 8

    #there are several other key-value pairs but we are concerned with these
    GENE_ID = "gene_id"
    TRANSCRIPT_ID = "transcript_id"
    GENE_TYPE = "gene_type"
    GENE_STATUS = "gene_status"
    GENE_NAME = "gene_name"
    TRANSCRIPT_TYPE = "transcript_type"
    TRANSCRIPT_STATUS = "transcript_status"
    TRANSCRIPT_NAME = "transcript_name"
    EXON_NUMBER = "exon_number"
    EXON_ID = "exon_id"
    LEVEL = "level"

    #some are missing
    TAG = "tag"

def load(path, gene_ids=None):
    results = []
    genes=set()
    F = GFTF
    for comps in Utilities.comps_from_file(path, skip_first=True):
        if "#" in comps[0]: continue

        feature_type = comps[F.FEATURE_TYPE]
        if feature_type != "gene": continue

        key_value_pairs = [x.translate(None, ';"') for x in comps[GFTF.KEY_VALUE_PAIRS:]]
        key_value_pairs = {key_value_pairs[2*i]:key_value_pairs[2*i+1] for i in xrange(0, len(key_value_pairs)/2)}

        gene_id = key_value_pairs[GFTF.GENE_ID]
        gene_name = key_value_pairs[GFTF.GENE_NAME]
        start = comps[GFTF.N_START_LOCATION]
        strand= comps[GFTF.GENOMIC_STRAND]
        if strand == ".": strand = None

        if gene_id in genes: continue

        r=(gene_id, gene_name, start, strand)
        results.append(r)
        genes.add(gene_id)

    results = zip(*results)
    results = {
        "gene_id":results[0],
        "gene_name":results[1],
        "start":results[2],
        "strand":results[3]
    }
    results = pandas.DataFrame(results)
    return results
