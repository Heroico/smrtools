import pandas
from .. import Utilities
from ..Constants import SNP

class TGTF(object):
    CHROMOSOME=0
    ID=1
    POSITION=2
    REF_ALLELE=3
    ALT_ALLELE=4
    FREQ_AFR=5
    FREQ_AMR=6
    FREQ_EAS=7
    FREQ_EUR=8
    FREQ_SAS=9
    FREQ_ALL=10

POP = {
    "EUR":TGTF.FREQ_EUR
}

def load_frequency(path, snps, populations=["EUR"]):
    results = []
    F = TGTF
    for comps in Utilities.comps_from_file(path, skip_first=True):
        rsid = comps[F.ID]
        if not rsid in snps: continue
        line = [rsid]
        freqs = [comps[POP[x]] for x in populations]
        line.extend(freqs)
        entry = tuple(line)
        results.append(entry)
    columns = [SNP]+[("FREQ_"+x).lower() for x in populations]
    results = zip(*results)
    results = {columns[i]:results[i] for i in xrange(0, len(results))}
    results = pandas.DataFrame(results)
    return results