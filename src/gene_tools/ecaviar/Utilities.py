import logging
import pandas
import ECAVIAR

from .. import Utilities
from ..dosage import Utilities as DosageUtilities
from .. misc import KeyedDataSource

class BasicContext(ECAVIAR.Context):
    def __init__(self, eqtl, reference_dosage_manager, eqtl_dosage_manager, gwas_path, ecaviar_exe, intermediate):
        self.eqtl = eqtl
        self.reference_dosage_manager = reference_dosage_manager
        self.eqtl_dosage_manager = eqtl_dosage_manager
        self.intermediate = intermediate
        self.eqtl_data = {}
        self.gwas_path = gwas_path
        self.gwas_data = {}
        self.chromosome_snps = set()
        self.chromosome_genes = set()
        self.ecaviar_exe = ecaviar_exe

    def get_gwas_dosage_manager(self):
        return self.reference_dosage_manager

    def get_eqtl_dosage_manager(self):
        return self.eqtl_dosage_manager

    def get_intermediate(self):
        return self.intermediate

    def get_ecaviar_exe_path(self):
        return self.ecaviar_exe

    def get_eqtl(self, gene_id=None):
        if gene_id is None:
            return self.eqtl

        if not gene_id in self.eqtl_data:
            return None

        return self.eqtl_data[gene_id]

    def get_loaded_genes(self):
        return self.chromosome_genes

    def get_gwas(self, rsids):
        rsids = [x for x in rsids if x in self.gwas_data]
        data = [(x, float(self.gwas_data[x])) for x in rsids]
        if len(data) == 0:
            return pandas.DataFrame({"rsid":[], "gwas_zscore":[]})
        data = Utilities.to_dataframe(data, ["rsid", "gwas_zscore"])
        return data

    def switch_chromosome_data(self, chromosome):
        logging.info("Switching to chromosome %s", str(chromosome))

        logging.log(9, "preparing eqtl data for chromosome %s", str(chromosome))
        self.eqtl_data, self.chromosome_snps, self.chromosome_genes = \
            _eqtl_data_for_chromosome(self.eqtl, chromosome)
        logging.log(9, "%d, %d, %d loaded", len(self.eqtl_data), len(self.chromosome_snps), len(self.chromosome_genes))

        logging.log(9, "preparing eqtl dosage for chromosome %s", str(chromosome))
        self.eqtl_dosage_manager.switch_chromosome(chromosome)
        logging.log(9, "%d loaded", len(self.eqtl_dosage_manager.data))

        logging.log(9, "preparing reference data for chromosome %s", str(chromosome))
        self.reference_dosage_manager.switch_chromosome(chromosome)
        logging.log(9,"%d loaded", len(self.reference_dosage_manager.data))

        logging.log(9, "preparing gwas data for chromosome %s", str(chromosome))
        self.gwas_data = KeyedDataSource.load_data(
            self.gwas_path, "rsid", "zscore", white_list=self.chromosome_snps)
        logging.log(9, "%d loaded", len(self.gwas_data))

def _eqtl_data_for_chromosome(eqtl, chromosome):
    #split by gene for performance
    eqtl = eqtl[eqtl.chromosome == chromosome]
    data = {}
    chromosome_snps = set()
    chromosome_genes = set()
    for row in eqtl.itertuples():
        gene_id = row.gene_id
        if not gene_id in data: data[gene_id] = []
        d = data[gene_id]
        d.append((row.chromosome, gene_id, row.rsid, row.variant_id, row.zscore))
        chromosome_snps.add(row.rsid)
        chromosome_genes.add(gene_id)

    keys = data.keys()
    for k in keys:
       data[k] = Utilities.to_dataframe(data[k], ["chromosome", "gene_id", "rsid", "variant_id", "eqtl_zscore"])

    return data, chromosome_snps, chromosome_genes

def load_eqtl(eqtl_path):
    eqtl = pandas.read_table(eqtl_path, sep="\s+")
    eqtl["key"] = eqtl.chromosome.astype(str)
    eqtl = eqtl.sort_values(by=["key", "gene_id"])
    eqtl.drop('key', axis=1, inplace=True)
    return eqtl

def context_from_args(args, standardize=False, remove_constants=True):
    logging.info("Loading eqtl")
    eqtl = load_eqtl(args.eqtl_path)

    rsids = set(eqtl.rsid)
    reference_dosage_manager = DosageUtilities.predixcan_dosage_manager(
        args.gwas_reference_folder, args.gwas_reference_pattern, rsids,
        standardize=standardize, remove_constants=remove_constants)

    variant_ids = set(eqtl.variant_id)
    gtex_dosage_manager = DosageUtilities.gtex_dosage_manager(
        args.eqtl_reference_file, variant_ids,
        standardize=standardize, remove_constants=remove_constants)

    context = BasicContext(eqtl, reference_dosage_manager, gtex_dosage_manager,args.gwas_path, args.ecaviar_path, args.intermediate)
    logging.info("Created context")
    return context