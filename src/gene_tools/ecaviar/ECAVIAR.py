import numpy
import os
import pandas
import subprocess
import shutil

from .. import Exceptions
from ..Constants import SNP
from ..Constants import ZSCORE

from .. import Utilities

def convert_gwas(gwas, frequency=None):
    rename = {
        SNP:"rsid",
        ZSCORE:"zscore"
    }
    d = gwas.rename(columns=rename)
    d = d.fillna("NA")
    d = d[["rsid","zscore"]]
    return d

class Context(object):
    def __init__(self):
        raise Exceptions.ReportableException("Tried to instantiate abstract ECAVIAR context")

    def get_eqtl(self, gene_id): pass
    def get_intermediate(self): pass
    def get_gwas_dosage_manager(self): pass
    def get_eqtl_dosage_manager(self): pass
    def get_gwas(self,ids): pass
    def get_ecaviar_exe_path(self): pass
    def get_loaded_genes(self): pass
    def switch_chromosome_data(self, chromosome):
        """the context loads only the data proper to a specific chromosome"""
        pass

def _build_cor(ids, dosage):
    keys, dosages = dosage.dosage_for_keys(ids)
    cor = numpy.corrcoef(dosages)
    return cor, keys, dosage

def _save_cor(cor, data, path):
    with open(path, "w") as file:
        n = len(data)
        if n > 1:
            for x in cor:
                line = "%s\n" % " ".join([str(y) for y in x])
                file.write(line)
        elif n == 1:
            line = "%s\n" % str(float(cor))

def _path(path, gene): return os.path.join(path, gene)
def _eqtlz(path): return os.path.join(path, "eqtl_z.txt")
def _gwasz(path): return os.path.join(path, "gwas_z.txt")
def _eqtlld(path): return os.path.join(path, "eqtl_ld.txt")
def _gwasld(path): return os.path.join(path, "gwas_ld.txt")
def _out(path): return os.path.join(path, "out_col")
def __out(path): return os.path.join(path, "out")

def save_data(path, data, eqtl_cor, gwas_cor):
    eqtl = data[["rsid", "eqtl_zscore"]]
    eqtl.to_csv(_eqtlz(path), index=False, header=False, sep="\t")

    gwas = data[["rsid", "gwas_zscore"]]
    gwas.to_csv(_gwasz(path), index=False, header=False, sep="\t")

    _save_cor(eqtl_cor, data, _eqtlld(path))
    _save_cor(gwas_cor, data, _gwasld(path))

def build_ecaviar_command(path, ecaviar_exe):
    command = "{}".format(ecaviar_exe)
    command += " -z {}".format(_gwasz(path))
    command += " -z {}".format(_eqtlz(path))
    command += " -l {}".format(_gwasld(path))
    command += " -l {}".format(_eqtlld(path))
    command += " -o {}".format(__out(path))
    return command

def build_ecaviar_data(context, gene):
    eqtl = context.get_eqtl(gene)
    gwas = context.get_gwas(eqtl.rsid.values)
    data = pandas.merge(eqtl, gwas, on="rsid", how="inner")
    if len(data) == 0:
        return None, None, None

    eqtl_dosage = context.get_eqtl_dosage_manager()
    eqtl_ids = eqtl_dosage.keys_in_dosage(set(data.variant_id))
    data = data[data.variant_id.isin(set(eqtl_ids))]

    gwas_dosage = context.get_gwas_dosage_manager()
    gwas_ids = gwas_dosage.keys_in_dosage(set(data.rsid))
    data = data[data.rsid.isin(set(gwas_ids))]

    if len(eqtl) == 0:
        return None, None, None

    gwas_cor, gwas_keys, gwas_dosages = _build_cor(data.rsid.values, gwas_dosage)
    eqtl_cor, eqtl_keys, eqtl_dosages = _build_cor(data.variant_id.values, eqtl_dosage)

    return data, eqtl_cor, gwas_cor

def process(context, gene):
    path = os.path.join(context.get_intermediate(), gene)
    os.makedirs(path)

    data, eqtl_cor, gwas_cor = build_ecaviar_data(context, gene)
    if data is None or len(data) == 0:
        shutil.rmtree(path)
        return [(gene, None, None)]

    save_data(path, data, eqtl_cor, gwas_cor)
    command = build_ecaviar_command(path, context.get_ecaviar_exe_path())
    rcode = subprocess.call(command, shell=True)

    if rcode != 0:
        shutil.rmtree(path)
        return [(gene, None, None)]

    output = _out(path)
    d = pandas.read_table(output, sep="\s+", names=["rsid", "blah", "clpp"])
    d = d.sort_values(by="clpp", ascending=False)

    results = []
    for row in d.itertuples():
        r = (gene, row.rsid, row.clpp)
        results.append(r)

    shutil.rmtree(path)

    return results

def results_to_data_frame(results):
    results = Utilities.to_dataframe(results, ["gene", "rsid", "clpp"])
    results = results.fillna("NA")
    return results

