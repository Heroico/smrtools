import os
import re
import logging
import gzip
import numpy

import DosageManager

from .. import Exceptions
from ..misc import Math

class PTF(object):
    """PrediXcan table format"""
    CHR=0
    ID=1
    POS=2
    REF_ALLELE=3
    EFF_ALLELE=4
    FREQ=5
    FIRST_DOSAGE=6

class PredixcanDosageManager(DosageManager.DosageManager):
    def __init__(self, folder, regexp, ids, standardize=False, remove_constants=False, maf=None):
        super(PredixcanDosageManager, self).__init__()
        self.folder = folder
        self.regexp = regexp
        self.keys = ids
        self.data = {}
        self.standardize = standardize
        self.maf=maf
        self.remove_constants = remove_constants

    def switch_chromosome(self, chromosome):
        self._load_cromosome(chromosome)

    def _load_cromosome(self, chromosome):
        logging.log(9, "PrediXcan Dosage Manager loading %s", str(chromosome))
        names = os.listdir(self.folder)
        names = {int(self.regexp.match(x).group(1)):os.path.join(self.folder,x) for x in names if self.regexp.match(x)}

        path = names[chromosome]
        data = _load_predixcan(path, self.keys, self.standardize, self.remove_constants, self.maf)
        self.data = data

def _add(data, key, dosage, standardize, remove_constants, maf=None):
    dosage = [float(x) for x in dosage]
    if maf is not None:
        m = numpy.mean(dosage) / 2
        if m < maf:
            return
        if m > 1 - maf:
            return

    if remove_constants:
        sd = numpy.std(dosage)
        if sd == 0:
            return

    if standardize:
        dosage = Math.standardize(dosage)
        if dosage is None:
            return

    if dosage is not None:
        data[key] = dosage

def _load_predixcan(path, ids, standardize=True, remove_constants=False, maf=None):
    data, metadata = {}, []
    with gzip.open(path) as file:
        for i, line in enumerate(file):
            comps = line.strip().split()
            id = comps[PTF.ID]
            if not id in ids:
                continue
            dosage = comps[PTF.FIRST_DOSAGE:]

            _add(data, id, dosage, standardize, remove_constants, maf)

    return data

def predixcan_dosage_manager(folder, pattern, ids, standardize=False, remove_constants=False, maf=None):
    regexp = re.compile(pattern)
    d = PredixcanDosageManager(folder, regexp, ids,
            standardize=standardize, maf=maf, remove_constants=remove_constants)
    return d

class GtexDosageManager(DosageManager.DosageManager):
    def __init__(self, path, ids, standardize=False, remove_constants=False):
        super(GtexDosageManager, self).__init__()
        self.path = path

        self.file = None
        self.keys = ids
        self.data = {}
        self.leftover_line = None
        self.standardize = standardize
        self.remove_constants = remove_constants

    def switch_chromosome(self, chromosome):
        self._load_cromosome(chromosome)

    def _load_cromosome(self, chromosome):
        self.data = {}

        if self.leftover_line:
            id = self.leftover_line[0]
            chrom = int(id.split("_")[0])
            if chromosome != chrom:
                logging.info("Need to reopen dosage, chromosome switch out of order")
                self.leftover_line = None
                if self.file:
                    self.file.close()
                    self.file = None
            else:
                _add(self.data, id, self.leftover_line[1], self.standardize, self.remove_constants)
                self.leftover_line = None

        if self.file is None:
            logging.log(9, "opening %s", str(chromosome))
            self.file = gzip.open(self.path)

        for i, line in enumerate(self.file):
            comps = line.strip().split()
            id = comps[0]

            if not id in self.keys:
                continue

            chrom = id.split("_")[0]
            dosage = comps[1:]
            if int(chrom) != chromosome:
                #if we haven loaded anything yet, skip the first lines
                if len(self.data) == 0:
                    continue
                #if we have read something and the line has a different cromosome to what we want, then go again
                else:
                    self.leftover_line = (id, dosage)
                    break

            _add(self.data, id, dosage, self.standardize, self.remove_constants)

        if not self.leftover_line:
            self.file.close()
            self.file =None

def gtex_dosage_manager(path, ids, standardize=False, remove_constants=False):
    dosage_manager = GtexDosageManager(path, ids,
                        standardize=standardize, remove_constants=remove_constants)
    return dosage_manager
