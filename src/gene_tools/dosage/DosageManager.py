from .. import Exceptions

class DosageManager(object):
    def __init__(self):
        self.data = None

    def dosage_for_keys(self, keys):
        keys = [x for x in keys if x in self.data]
        dosage = [self.data[x] for x in keys]
        return keys, dosage

    def keys_in_dosage(self, keys):
        keys = [x for x in keys if x in self.data]
        return keys

    def switch_chromosome(self, chromosome):
        """The dosage manager will only load specific chromosome data"""
        raise Exceptions.ReportableException("Call unimplemented method in abstract context")
