import os
import re
import gzip
import pandas
import logging

def candidate_path(folder, name):
    candidates = [x for x in os.listdir(folder) if name in x]
    if len(candidates) != 1: raise RuntimeError("Won't pick candidate %s, %s", folder, name)
    path = os.path.join(folder, candidates[0])
    return path

def files_in_folder(folder, pattern=None):
    contents = os.listdir(folder)
    if pattern:
        regexp = re.compile(pattern)
        contents = [c for c in contents if regexp.match(c)]
    contents = [os.path.join(folder, c) for c in contents]
    return contents

def comps_from_file(path, return_path=False, skip_first=False):
    with gzip.open(path) as file:
        if skip_first: file.readline()
        if return_path:
            for line in file:
                yield path, line.strip().split()
        else:
            for line in file:
                yield line.strip().split()

def ensure_requisite_folders(path):
    folder = os.path.split(path)[0]
    if len(folder) and not os.path.exists(folder):
        os.makedirs(folder)

def to_dataframe(data, columns,to_numeric=None, fill_na=None):
    data = zip(*data)
    if to_numeric:
        data = [pandas.to_numeric(x, errors=to_numeric) for x in data]
    data = {columns[i]:data[i] for i in xrange(0, len(columns))}
    data = pandas.DataFrame(data)
    data = data[columns]
    if fill_na:
        data = data.fillna(fill_na)
    return data

class PercentReporter(object):
    def __init__(self, level, total, increment=10, pattern="%i %% complete"):
        self.level = level
        self.total = total
        self.increment = increment
        self.last_reported = 0
        self.pattern = pattern

    def update(self, i, text=None, force=False):
        percent = int(i*100.0/self.total)

        if force or  percent >= self.last_reported + self.increment:
            self.last_reported = percent

            if not text:
                text = self.pattern

            logging.log(self.level, text, percent)

txt_gz_re = re.compile("(.*).txt.gz$")

ma_re = re.compile("(.*)\.ma$")
def pheno_name_heuristic(pheno):
    pheno = os.path.split(pheno)[1]
    if ma_re.search(pheno):
        pheno = ma_re.search(pheno).group(1)
    return pheno

eqtl_full_re = re.compile("(.*)_Analysis.v6p.all_snpgene_pairs.txt.gz$")
def tissue_name_heuristic(tissue):
    tissue = os.path.split(tissue)[1]
    if eqtl_full_re.search(tissue):
        tissue = eqtl_full_re.search(tissue).group(1)
    elif txt_gz_re.search(tissue):
        tissue = txt_gz_re.search(tissue).group(1)
    return tissue
