import gzip

def load_multi_index(path):
    f_r = gzip.open if "gz" in path else open
    r = {}
    with f_r(path) as file:
        for i, line in enumerate(file):
            if i == 0: continue
            comps = line.strip().split()
            cursor = None

            c = len(comps)
            for i,comp in enumerate(comps):
                if i==0:
                    if not comp in r:
                        r[comp] = {}
                    cursor = r[comp]
                    continue

                if i==c-1:
                    cursor[comp] = True
                    continue

                if not comp in cursor:
                    cursor[comp] = {}
                cursor = cursor[comp]
    return r

