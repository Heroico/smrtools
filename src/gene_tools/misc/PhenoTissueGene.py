import re
import os
import pandas

from .. import Utilities

def load_pheno_tissue_gene(path):
    d = pandas.read_table(path)
    return d

def interrogate(pheno_tissue_gene, pheno=None, tissue=None):
    if not pheno:
        return

    pheno = Utilities.pheno_name_heuristic(pheno)
    pheno_tissue_gene = pheno_tissue_gene[pheno_tissue_gene.pheno == pheno]

    if not tissue:
        tissues = set(pheno_tissue_gene.tissue.values)
        return tissues

    tissue = Utilities.tissue_name_heuristic(tissue)
    pheno_tissue_gene = pheno_tissue_gene[pheno_tissue_gene.tissue == tissue]
    genes = set(pheno_tissue_gene.gene.values)
    return genes

