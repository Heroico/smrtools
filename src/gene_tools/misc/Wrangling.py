import pandas

from .. import Constants

def align_data_to_alleles(data, base, left_on, right_on, flips=[], how="inner"):
    EA, NEA = Constants.EFFECT_ALLELE, Constants.NON_EFFECT_ALLELE
    EA_BASE, NEA_BASE = EA+"_BASE", NEA+"_BASE"
    merged = pandas.merge(data, base, left_on=left_on, right_on=right_on, suffixes=("", "_BASE"), how=how)

    alleles_1 = pandas.Series([set(e) for e in zip(merged[EA], merged[NEA])])
    alleles_2 = pandas.Series([set(e) for e in zip(merged[EA_BASE], merged[NEA_BASE])])

    if how == "inner":
        eq = alleles_1 == alleles_2
        merged = merged[eq]
        flip_at = merged[EA] != merged[EA_BASE]
    else:
        flip_at = (merged[EA] != merged[EA_BASE]) & (~merged[EA].isnull())

    for flip in flips:
        if flip in merged:
            merged.loc[flip_at, flip] = - merged.loc[flip_at, flip]

    merged[EA] = merged[EA_BASE]
    merged[NEA] = merged[NEA_BASE]
    merged = merged.drop(EA_BASE, axis=1).drop(NEA_BASE, axis=1)

    return merged