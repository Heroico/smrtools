import logging
import pandas

from ..GTEx import GTExEQTLsignif
from ..GTEx import GTExSNPFile
from ..GTEx import GTExFreq

def load_annotated(path, snp_path, frequency_path):
    logging.info("Loading eQTL")
    eqtl = GTExEQTLsignif.load(path)

    logging.info("Loading snps")
    snp = GTExSNPFile.load(snp_path, variants=set(eqtl.variant_id.values))

    merged = pandas.merge(eqtl, snp, how="inner", on="variant_id")
    merged = merged.sort_values(by="gene_id")

    if frequency_path:
        logging.info("Loading snp frequency")
        frequency = GTExFreq.load_freq(frequency_path)
        merged = pandas.merge(merged, frequency, how="inner", on="variant_id")

    return merged