import numpy

def standardize(x, maf_0_2=None):
    mean = numpy.mean(x)
    if maf_0_2:
        m = mean/2
        if m < maf_0_2:
            return None
        if m > 1-maf_0_2:
            return None

    scale = numpy.std(x)
    if scale == 0:
        return None

    x = x - mean
    x = x / scale
    return x