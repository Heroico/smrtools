import pandas
from .. import Utilities

class SNPTF(object):
    Chr=0
    Pos=1
    VariantID=2
    Ref_b37=3
    Alt=4
    RS_ID_dbSNP135_original_VCF=5
    RS_ID_dbSNP142_CHG37p13=6
    Num_alt_per_site=7

    HEADER="Chr\tPos\tVariantID\tRef_b37 Alt\tRS_ID_dbSNP135_original_VCF\tRS_ID_dbSNP142_CHG37p13\tNum_alt_per_site"

def load(path, snps=None, variants=None):
    results = []
    F = SNPTF
    for comps in Utilities.comps_from_file(path, skip_first=True):
        variant=comps[F.VariantID]
        if variants and not variant in variants: continue

        rsid = comps[F.RS_ID_dbSNP142_CHG37p13]
        if snps and not rsid in snps: continue

        num_alt = comps[F.Num_alt_per_site]
        if not num_alt == "1": continue

        non_effect_allele = comps[F.Ref_b37]
        if len(non_effect_allele) != 1: continue

        effect_allele = comps[F.Alt]
        if len(effect_allele) != 1: continue

        r = (rsid, variant, non_effect_allele, effect_allele, comps[F.Chr], comps[F.Pos])
        results.append(r)

    results = zip(*results)
    results = {
        "rsid":results[0],
        "variant_id":results[1],
        "non_effect_allele":results[2],
        "effect_allele": results[3],
        "chromosome":results[4],
        "position":results[5]
    }
    results = pandas.DataFrame(results)
    return results
