import pandas
import numpy
from .. import Utilities

class GTEXEQTLF(object):
    VARIANT_ID = 0
    GENE_ID = 1
    TSS_DISTANCE = 2
    PVAL_NOMINAL = 3
    SLOPE = 4
    SLOPE_SE = 5
    SLOPE_FPKM = 6
    SLOPE_FPKM_SE = 7
    PVAL_NOMINAL_THRESHOLD = 8
    MIN_PVAL_NOMINAL = 9
    PVAL_BETA = 10

def load(path):
    results = []
    F = GTEXEQTLF
    for comps in Utilities.comps_from_file(path, skip_first=True):
        results.append((comps[F.VARIANT_ID], comps[F.GENE_ID], comps[F.SLOPE], comps[F.SLOPE_SE]))
    results = zip(*results)
    results = {
        "variant_id":results[0],
        "gene_id":results[1],
        "beta":numpy.array(results[2], dtype=numpy.float64),
        "se":numpy.array(results[3], dtype=numpy.float64)
    }
    results = pandas.DataFrame(results)
    return results

def variants(path):
    results = []
    F = GTEXEQTLF
    for comps in Utilities.comps_from_file(path, skip_first=True):
        results.append(comps[F.VARIANT_ID])
    results = set(results)
    return results

