import pandas

def load_freq(path):
    frequency = pandas.read_table(path)
    frequency = frequency.rename(columns={"variant":"variant_id"})
    return frequency