import gzip
import math

import pandas

#Todo: migrate to below
def fequencies(path, variants):
    v = []
    f = []
    with gzip.open(path) as file:
        for i, line in enumerate(file):
            if i==0: continue
            comps = line.strip().split()

            variant = comps[0]
            if not variant in variants: continue

            freqs = map(float, comps[1:])
            freq = math.fsum(freqs)/(2*len(freqs))

            v.append(variant)
            f.append(freq)

    result = pandas.DataFrame({"variant":v, "frequency":f})
    result = result[["variant","frequency"]]
    return result

def _process_line(line, variants):
    comps = line.strip().split()
    variant = comps[0]
    if variants and not variant in variants:
        return None, None

    freqs = map(float, comps[1:])
    freq = math.fsum(freqs) / (2 * len(freqs))
    return variant, freq


def frequencies_iterator(path, variants=None):
    with gzip.open(path) as file:
        for i, line in enumerate(file):
            if i==0:
                continue

            variant, freq = _process_line(line, variants)

            if variant is None:
                continue

            yield variant, freq
