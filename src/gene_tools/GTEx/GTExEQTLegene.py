import pandas

class EGTF(object):
    K_GENE_ID = "gene_id"
    K_GENE_NAME = "gene_name"
    K_GENE_CHR = "gene_chr"
    K_GENE_START = "gene_start"
    K_STRAND="strand"

    COLUMNS=[K_GENE_ID, K_GENE_NAME, K_GENE_CHR, K_GENE_START, K_STRAND]

def load_egene(path):
    d = pandas.read_csv(path)
    d = d[EGTF.COLUMNS]
    return d