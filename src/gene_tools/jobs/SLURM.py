import logging
import time
import sys
import os
from subprocess import Popen, PIPE, call

def job_header(job_name, logs_folder="logs", partition="broadwl", time="2:00:00", mem="4096", ntasks="1"):

    header = "#!/bin/bash \n"
    header += "#SBATCH --job-name={} \n".format(job_name)

    if logs_folder:
        log_name = os.path.join(logs_folder, job_name)+"_${SLURM_JOB_ID}.log"
        err_name = os.path.join(logs_folder, job_name) + "_${SLURM_JOB_ID}.err"

        header += "#SBATCH --output={} \n".format(log_name)
        header += "#SBATCH --error={} \n".format(err_name)
# SBATCH --array=1-22
    header+="#SBATCH --partition={} \n".format(partition)
    header+="#SBATCH --mem-per-cpu={} \n".format(mem)
    header+="#SBATCH --ntasks={} \n".format(ntasks)
    header+="#SBATCH --time={} \n".format(time)
    return header


def submit_command(job_folder, job_name, command, fake=False, serialize_local=False):
    path = os.path.join(job_folder, job_name+ ".sbatch")

    if os.path.exists(path):
        logging.info("job %s already exists, we assume it to be queued in the past", job_name)
        return None

    with open(path, "w") as file:
        file.write(command)

    if fake: return job_name

    if serialize_local:
        logging.info("Running %s", path)
        call(["bash", path])
        return job_name

    return submit_job(path, job_name=job_name)

def submit_job(path ,job_name=None):
    job_name = os.path.split(path)[1] if not job_name else job_name
    retry = 0
    command = ["sbatch",path]
    def submit():
        logging.log(8,"Submitting Command: %s", " ".join(command))
        proc = Popen(command,stdout=PIPE, stderr=PIPE)
        out, err = proc.communicate()
        exitcode = proc.returncode
        return exitcode, out, err

    exitcode, out, err = submit()
    while exitcode != 0 and retry < 10:
        logging.info("retry %s %i-th attempt", job_name, retry)
        exitcode, out, err = submit()
        retry += 1
        time.sleep(0.5)
    time.sleep(0.5)
    r=None
    if exitcode==0:
        r=out.strip()
        r=str(r)
        r=r.split("Submitted batch job ")[1]

    return r