


# Eventually this should be configurable or something
# Yet it also depends on unpredictable stuff.
# Since so many traits might share a common prefix, a regexp file name matching is not enough, or they should be assemble dvery carefully
# So, we should probably do this with trait file name

def decide_mem(file):
    if "EGG_BW3" in file: return "16gb"
    return "8gb"

def decide_walltime(file, besd_path, job_name):
    if "EGG_BMI_HapMapImputed_eQTL_Brain_Nucleus_accumbens_basal_ganglia_Analysis" in job_name or \
        "GIANT_BMI_WOMEN" in job_name or \
        "GIANT_HEIGHT" in job_name or \
        "GIANT_HIP" in job_name or \
        "GIANT_WC" in job_name or \
        "GIANT_WHR" in job_name or \
        "GLGC_Mc_TG" in job_name:
        return "12:00:00"

    return "6:00:00"



