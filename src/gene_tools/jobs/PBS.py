import os
import logging
from subprocess import Popen, PIPE, call
import time

def job_header(job_name, inner_clause=None, email=None, walltime="2:00:00", job_mem="4gb", log_folder="logs", module_clause=None):
    header = "#!/bin/bash" + "\n"
    header += "#PBS -N " + job_name + "\n"
    if inner_clause:
        header += inner_clause + "\n"
    if email:
        header += "#PBS -M {} \n".format(email)
        header += "#PBS -m a \n"
    header += "#PBS -S /bin/bash \n"
    header += "#PBS -l walltime={} \n".format(walltime)
    header += "#PBS -l nodes=1:ppn=1 \n"
    header += "#PBS -l mem={} \n".format(job_mem)
    header += "#PBS -o {} \n".format(os.path.join(log_folder, "${PBS_JOBNAME}.o${PBS_JOBID}.log"))
    header += "#PBS -e {} \n".format(os.path.join(log_folder, "${PBS_JOBNAME}.e${PBS_JOBID}.err"))
    header += "cd $PBS_O_WORKDIR \n\n"
    if module_clause:
        header += module_clause + "\n"
    header += "\n"
    return header

def submit_command(job_folder, job_name, command, fake=False, serialize_local=False):
    path = os.path.join(job_folder, job_name+ ".sh")

    if os.path.exists(path):
        logging.info("job %s already exists, we assume it to be queued in the past", job_name)
        return

    with open(path, "w") as file:
        file.write(command)

    if serialize_local:
        logging.info("Running %s", path)
        call(["bash", path])
        return

    if fake: return job_name

    return submit_job(path, job_name=job_name)

def submit_job(path ,job_name=None):
    job_name = os.path.split(path)[1] if not job_name else job_name
    retry = 0
    command = ["qsub",path]
    def submit():
        logging.log(8,"Submitting Command: %s", " ".join(command))
        proc = Popen(command,stdout=PIPE, stderr=PIPE)
        out, err = proc.communicate()
        exitcode = proc.returncode
        return exitcode, out, err

    exitcode, out, err = submit()
    while exitcode != 0 and retry < 10:
        logging.info("retry %s %i-th attempt", job_name, retry)
        exitcode, out, err = submit()
        retry += 1
        time.sleep(0.1)
    time.sleep(0.1)
    return out.strip() if exitcode==0 else None
