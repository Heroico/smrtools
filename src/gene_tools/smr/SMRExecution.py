

def smr_besd(esd_prefix, output_prefix, smr_command="smr_Linux"):
    command = smr_command + " \\\n"
    command += "--eqtl-flist %s \\\n" % (esd_prefix)
    command += "--make-besd --out %s" % output_prefix
    return command