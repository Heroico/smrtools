import pandas

from scipy import stats

from ..Constants import SNP
from ..Constants import EFFECT_ALLELE
from ..Constants import NON_EFFECT_ALLELE
from ..Constants import BETA
from ..Constants import SE
from ..Constants import ZSCORE
from ..Constants import PVALUE
from ..Constants import FREQUENCY
from ..Constants import N

def convert_gwas(gwas, frequency=None):
    if not SE in gwas:
        gwas[SE] = gwas.beta/gwas.zscore if BETA in gwas else None

    if not FREQUENCY in gwas:
        if frequency is None:
            gwas[FREQUENCY] = None
        else:
            gwas = pandas.merge(gwas, frequency, on=SNP, how="left")

    if not N in gwas:
        gwas[N] = None

    rename = {
        SNP:"SNP",
        EFFECT_ALLELE:"A1",
        NON_EFFECT_ALLELE:"A2",
        FREQUENCY:"freq",
        BETA:"b",
        SE:"se",
        PVALUE:"p",
        N:"n"
    }
    d = gwas.rename(columns=rename)
    d = d.fillna("NA")
    d = d[["SNP","A1","A2", "freq", "b", "se", "p", "n"]]
    return d