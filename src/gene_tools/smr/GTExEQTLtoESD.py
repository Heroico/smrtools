import os
import logging

import numpy
import pandas

from scipy import stats

from .. import Utilities

from ..GTEx import GTExSNPFile
from ..GTEx import GTExEQTLsignif
from ..GTEx import GTExFreq
from ..Gencode import Gencode
from ..misc import DataFrameStreamer

def build_esd_data(data):
    rename = {
        "chromosome":"Chr",
        "rsid":"SNP",
        "position":"Bp",
        "effect_allele":"A1",
        "non_effect_allele":"A2",
        "beta":"Beta",
        "se":"se",
        "frequency":"Freq"
    }
    f = data.rename(columns=rename)
    f["p"] = 2*stats.norm.cdf(-numpy.absolute(f.Beta/f.se))
    f = f[["gene_id", "Chr", "SNP", "Bp", "A1", "A2", "Freq", "Beta", "se", "p"]]
    #SMR doesnt like {CG} or {AT} pairs
    # k = f.A1 + f.A2
    # k = k.apply(set)
    # k = k.apply(lambda x:  x!={"C","G"} and x != {"A","T"})
    # f = f[k]
    f = f[f.SNP != "."]
    return f

def serialize_esd(esd, working_path):
    if not os.path.exists(working_path): os.makedirs(working_path)
    results = {}
    for row in esd.values:
        gene_id = row[0]
        entry = row[1:]
        if not gene_id in results: results[gene_id] = []
        entries = results[gene_id]
        entries.append(entry)

    gene_ids = []
    paths = []
    for gene_id, entries in results.iteritems():
        path = esd_file_path(working_path, gene_id)
        Utilities.ensure_requisite_folders(path)
        with open(path,"w") as file:
            header = _to_line(esd.columns[1:])
            file.write(header)
            for entry in entries:
                line = _to_line(entry)
                file.write(line)
            gene_ids.append(gene_id)
            paths.append(path)

    esd_paths = pandas.DataFrame({"gene_id":gene_ids,"path":paths})
    return esd_paths

def build_flist_data(merged, esd_paths):
    merged = pandas.merge(merged, esd_paths, on="gene_id")
    rename = {
        "chromosome":"Chr",
        "gene_id":"ProbeId",
        "start":"ProbeBp",
        "gene_name":"Gene",
        "strand":"Orientation",
        "path":"PathOfEsd"
    }
    f = merged.rename(columns= rename)
    f["GeneticDistance"] = 0
    f = f[["Chr", "ProbeId", "GeneticDistance", "ProbeBp", "Gene", "Orientation", "PathOfEsd"]]
    f = f.drop_duplicates()
    f = f.fillna("NA")
    f = f.sort_values(by="ProbeBp")
    return  f

def working_root(working_path):
    dirname, tail = os.path.split(working_path)
    root = os.path.join(working_path, tail)
    return root

def esd_file_path(working_path, name):
    e = os.path.join(working_root(working_path), name) + ".esd"
    return e

def flist_path(working_path):
    f = working_root(working_path) + ".flist"
    return f

def _to_line(comps):
    comps = [str(x) for x in comps]
    line = "\t".join(comps)+"\n"
    return line

def build_esd_files(working_path, gtex_eqtl_path, gtex_snp_path, gtex_frequency_path, gencode_path):
    logging.info("Loading eQTL")
    eqtl = GTExEQTLsignif.load(gtex_eqtl_path)

    _build_esd_files(working_path, eqtl, gtex_snp_path, gtex_frequency_path, gencode_path)

def build_esd_files_filtered(working_path, gtex_eqtl_path, gtex_snp_path, gtex_frequency_path, gencode_path, white_list):
    logging.info("Loading filtered eqtl")
    eqtl = DataFrameStreamer.load_filtered_data_frame(gtex_eqtl_path, "gene_id", white_list, ".",
                rename_columns={"slope":"beta", "slope_se":"se"}, columns_filter=["gene_id", "variant_id", "beta", "se"])
    _build_esd_files(working_path, eqtl, gtex_snp_path, gtex_frequency_path, gencode_path)

def _build_esd_files(working_path, eqtl, gtex_snp_path, gtex_frequency_path, gencode_path):
    logging.info("Loading snps")
    snp = GTExSNPFile.load(gtex_snp_path, variants=set(eqtl.variant_id.values))

    logging.info("Loading snp frequency")
    frequency = GTExFreq.load_freq(gtex_frequency_path)

    merged = pandas.merge(eqtl, snp, how="inner", on="variant_id")
    merged = pandas.merge(merged, frequency, how="inner", on="variant_id")

    logging.info("Loading Gencode")
    gencode = Gencode.load(gencode_path, gene_ids=set(merged.gene_id.values))

    logging.info("Processing")
    Utilities.ensure_requisite_folders(working_path)

    merged = pandas.merge(merged, gencode, on="gene_id", how="inner")

    esd = build_esd_data(merged)
    esd_paths = serialize_esd(esd, working_path)

    f_p = flist_path(working_path)

    logging.info("Building esd files")
    f = build_flist_data(merged, esd_paths)
    f.to_csv(f_p, sep="\t", index=False)

    logging.info("Successfully built esd files")