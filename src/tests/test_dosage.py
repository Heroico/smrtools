
import unittest

import numpy
import numpy.testing

import gene_tools
from gene_tools.dosage import DosageManager
from gene_tools.dosage import Utilities

class TestDosageManager(unittest.TestCase):

    def test_predixcan(self):
        snps = set(["rs1", "rs2", "rs3", "rs12", "rs13", "rs14"])
        manager = Utilities.predixcan_dosage_manager("tests/_td/filtered_dosage", "chr([0-9]+).dosage.gz", snps, maf=None)

        keys, dosages = manager.dosage_for_keys(["rs1", "rs3", "rs4"])
        self.assertEqual(keys, [])
        self.assertEqual(dosages, [])

        manager.switch_chromosome(1)

        keys, dosages = manager.dosage_for_keys(["rs3", "rs1", "rs4"])
        self.assertEqual(["rs3", "rs1"], keys)
        self.assertEqual(len(dosages), 2)
        numpy.testing.assert_almost_equal(dosages[0], [0.0, 1.0, 0.0, 1.0])
        numpy.testing.assert_almost_equal(dosages[1], [0.0, 0.0, 0.0, 2.0])

        keys, dosages = manager.dosage_for_keys(["rs11", "rs14"])
        self.assertEqual(keys, [])
        self.assertEqual(dosages, [])

        manager.switch_chromosome(12)

        keys, dosages = manager.dosage_for_keys(["rs11", "rs14"])
        self.assertEqual(["rs14"], keys)
        self.assertEqual(len(dosages), 1)
        numpy.testing.assert_almost_equal(dosages[0], [0.0, 0.0, 0.0, 0.0])

        snps = set(["rs1", "rs2", "rs3", "rs12", "rs13", "rs14"])
        manager = Utilities.predixcan_dosage_manager("tests/_td/filtered_dosage", "chr([0-9]+).dosage.gz", snps, maf=0.3)

        manager.switch_chromosome(1)

        keys, dosages = manager.dosage_for_keys(["rs2", "rs3", "rs4"])
        self.assertEqual(["rs2"], keys)
        self.assertEqual(len(dosages), 1)
        numpy.testing.assert_almost_equal(dosages[0], [1.0, 1.0, 1.0, 1.0])

        manager.switch_chromosome(12)

        keys, dosages = manager.dosage_for_keys(["rs11", "rs14"])
        self.assertEqual([], keys)
        self.assertEqual(len(dosages), 0)


    def test_gtex_like(self):
        ids = set(["1_1", "1_2", "1_3", "1_4", "1_6", "12_1", "12_6", "12_7", "12_8"])
        manager = Utilities.gtex_dosage_manager("tests/_td/gtex_like_dosage/geno.txt.gz", ids)
        manager.switch_chromosome(1)

        keys, dosages = manager.dosage_for_keys(["1_1", "1_2", "1_3", "1_4", "1_6"])
        self.assertEqual(keys, ["1_1", "1_2", "1_3", "1_4", "1_6"])
        self.assertEqual(len(dosages), 5)
        numpy.testing.assert_almost_equal(dosages[0], [0.0, 0.1, 0.0, 0.1, 0.0, 0.0])
        numpy.testing.assert_almost_equal(dosages[1], [0.0, 0.5, 0.5, 0.6, 1.0, 0.6])
        numpy.testing.assert_almost_equal(dosages[2], [0.0, 1.0, 2.0, 1.0, 2.0, 1.0])
        numpy.testing.assert_almost_equal(dosages[3], [0.0, 0.9, 0.1, 0.3, 1.7, 1.9])
        numpy.testing.assert_almost_equal(dosages[4], [2.0, 1.5, 1.0, 0.9, 0.3, 0.0])

        manager.switch_chromosome(1)

        keys, dosages = manager.dosage_for_keys(["1_1", "1_2", "1_3", "1_4", "1_6"])
        self.assertEqual(keys, ["1_1", "1_2", "1_3", "1_4", "1_6"])
        self.assertEqual(len(dosages), 5)
        numpy.testing.assert_almost_equal(dosages[0], [0.0, 0.1, 0.0, 0.1, 0.0, 0.0])
        numpy.testing.assert_almost_equal(dosages[1], [0.0, 0.5, 0.5, 0.6, 1.0, 0.6])
        numpy.testing.assert_almost_equal(dosages[2], [0.0, 1.0, 2.0, 1.0, 2.0, 1.0])
        numpy.testing.assert_almost_equal(dosages[3], [0.0, 0.9, 0.1, 0.3, 1.7, 1.9])
        numpy.testing.assert_almost_equal(dosages[4], [2.0, 1.5, 1.0, 0.9, 0.3, 0.0])
        manager.switch_chromosome(12)

        keys, dosages = manager.dosage_for_keys(["12_1", "12_2", "12_6", "12_8", "12_7"])
        self.assertEqual(keys, ["12_1", "12_6", "12_8", "12_7"])
        self.assertEqual(len(dosages), 4)
        numpy.testing.assert_almost_equal(dosages[0], [0.4, 0.6, 0.4, 0.3, 0.5, 0.4])
        numpy.testing.assert_almost_equal(dosages[1], [0.0, 1.0, 0.0, 0.1, 0.0, 2.0])
        numpy.testing.assert_almost_equal(dosages[2], [0.0, 0.0, 0.0, 0.1, 0.0, 0.0])
        numpy.testing.assert_almost_equal(dosages[3], [0.0, 0.1, 0.3, 0.1, 0.7, 0.9])

if __name__ == '__main__':
    unittest.main()