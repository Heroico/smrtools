import os
import shutil

import unittest

import numpy
import numpy.testing

import gene_tools
from gene_tools.ecaviar import Utilities
from gene_tools.ecaviar import ECAVIAR
from gene_tools import Utilities as U

class Dummy(object):
    def __init__(self):
        self.eqtl_path = "tests/_td/ecaviar/eqtl/eqtl_test.txt.gz"
        self.eqtl_reference_file = "tests/_td/gtex_like_dosage/geno.txt.gz"
        self.gwas_path = "tests/_td/ecaviar/gwas/gwas.txt.gz"
        self.gwas_reference_folder = "tests/_td/filtered_dosage"
        self.gwas_reference_pattern = "chr([0-9]+).dosage.gz"
        self.intermediate = "tests/_intermediate"
        self.ecaviar_path = ""

class TestCaviarTools(unittest.TestCase):
    def setUp(self):
        if os.path.exists("tests/_intermediate"):
            shutil.rmtree("tests/_intermediate")

    def test(self):
        args = Dummy()
        context = Utilities.context_from_args(args, standardize=False, remove_constants=True)
        context.switch_chromosome_data(1)
        data, eqtl_cor, gwas_cor = ECAVIAR.build_ecaviar_data(context, "A")
        numpy.testing.assert_almost_equal(eqtl_cor,
            [[ 1.        , -0.17149859],
            [-0.17149859,  1.        ]])

        numpy.testing.assert_almost_equal(gwas_cor,
            [[ 1.        ,  0.57735027],
            [ 0.57735027,  1.        ]])

        d = U.to_dataframe(
            [(1, "A", "rs1", "1_1", -4.0, -2.0),
             (1, "A", "rs3", "1_3", -4.2, -4.0)],
            ["chromosome", "gene_id", "rsid", "variant_id",  "eqtl_zscore", "gwas_zscore"])

        numpy.testing.assert_array_equal(data.rsid.values, d.rsid.values)
        numpy.testing.assert_array_equal(data.variant_id.values, d.variant_id.values)
        numpy.testing.assert_array_equal(data.gene_id.values, d.gene_id.values)
        numpy.testing.assert_array_equal(data.chromosome, d.chromosome)
        numpy.testing.assert_array_almost_equal(data.eqtl_zscore, d.eqtl_zscore)
        numpy.testing.assert_array_almost_equal(data.gwas_zscore, d.gwas_zscore)

if __name__ == '__main__':
    unittest.main()