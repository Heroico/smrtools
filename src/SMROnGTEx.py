#! /usr/bin/env python

import os
import shutil
import subprocess
import logging

from gene_tools import Logging
from gene_tools import Utilities
from gene_tools.misc import PhenoTissueGene
from gene_tools.smr import GTExEQTLtoESD
from gene_tools.smr import SMRExecution

def _esd_path(args):
    return os.path.join(args.working_folder, "esd")

def _besd_path(args):
    return os.path.join(args.working_folder, "besd", "besd")

def build_esd(args):
    logging.info("Loading filter")
    index_filter = PhenoTissueGene.load_pheno_tissue_gene(args.index_filter)
    genes = PhenoTissueGene.interrogate(index_filter, args.gwas_path, args.gtex_eqtl_path)
    logging.info("%d genes in whitelist", len(genes))

    logging.info("Converting eQTL")
    esd_path = _esd_path(args)
    GTExEQTLtoESD.build_esd_files_filtered(esd_path, args.gtex_eqtl_path, args.gtex_snp_path, args.gtex_frequency_path, args.gencode_path, genes)

def smr_to_besd(args):
    logging.info("Running SMR BESD conversion")
    esd_path = _esd_path(args)
    flist = GTExEQTLtoESD.flist_path(esd_path)

    besd_output = _besd_path(args)
    Utilities.ensure_requisite_folders(besd_output)
    command = SMRExecution.smr_besd(flist, besd_output, args.smr_command)

    smr_log_target = "/dev/null" if not args.smr_log_prefix else args.smr_log_prefix + ".besd.o"
    command += " > {} \n".format(smr_log_target)

    subprocess.call(command,shell=True)

def run_smr(args):
    logging.info("Running SMR analysis")
    besd_path = _besd_path(args)
    Utilities.ensure_requisite_folders(args.output_prefix)

    command = "{} \\\n".format(args.smr_command)
    command += "--gwas-summary {} \\\n".format(os.path.join(args.gwas_path))
    command += "--bfile {} \\\n".format(args.reference)
    command += "--beqtl-summary {} \\\n".format(besd_path)
    command +=  "--out {}".format(args.output_prefix)

    smr_log_target = "/dev/null" if not args.smr_log_prefix else args.smr_log_prefix + ".smr.o"
    command += " > {} \n".format(smr_log_target)
    subprocess.call(command, shell=True)


def run(args):
    if os.path.exists(args.working_folder):
        logging.info("%s already exists. Delete it or move it if you want it done again", args.working_folder)
        return
    os.makedirs(args.working_folder)

    build_esd(args)
    smr_to_besd(args)
    run_smr(args)

    logging.info("Cleaning up")
    shutil.rmtree(args.working_folder)

    logging.info("Ran SMR")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("run SMR on full GTEx file")

    parser.add_argument("--index_filter", help="path to index filter")
    parser.add_argument("--reference", help="prefix of reference panel")
    parser.add_argument("--gwas_path", help="path to gwas file")
    parser.add_argument("--gtex_eqtl_path", help="path to GTEx eqtl file")
    parser.add_argument("--gtex_frequency_path", help="path to gtex frequency")
    parser.add_argument("--gtex_snp_path", help="path to GTEx snp file")
    parser.add_argument("--gencode_path", help="path to Gencode file")
    parser.add_argument("--working_folder", help="path where SMR ESD will be built")
    parser.add_argument("--smr_command", help="what smr executable to use")
    parser.add_argument("--output_prefix", help="SMR prefix output")
    parser.add_argument("--smr_log_prefix", help= "Prefix to use with the smr logs")

    parser.add_argument("--verbosity",
                        help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything",
                        default = "10")

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)