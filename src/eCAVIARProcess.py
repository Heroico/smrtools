#! /usr/bin/env python

import os
import logging

from gene_tools import Logging
from gene_tools import Utilities

from gene_tools.ecaviar import Utilities as ECAVIARUtilities
from gene_tools.ecaviar import ECAVIAR

def process_context(context, chromosome):
    context.switch_chromosome_data(chromosome)
    genes = context.get_loaded_genes()
    results = []
    reporter = Utilities.PercentReporter(9, len(genes))
    reporter.update(0, "%d %% of chromosome's genes processed so far")
    for i,gene in enumerate(genes):
        reporter.update(i, "%d %% of chromosome's genes processed so far")
        r = ECAVIAR.process(context, gene)
        results.extend(r)
    return results

def run(args):
    if os.path.exists(args.output):
        logging.info("%s already exists. Delete it or move it if you want it done again", args.output)
        return

    logging.info("Creating context")
    context = ECAVIARUtilities.context_from_args(args)

    #chromosomes = [int(i) for i in sorted([str(x) for x in xrange(1,23)])]
    chromosomes = xrange(1, 23)
    results = []
    for chromosome in chromosomes:
        logging.info("Processing %s", chromosome)
        r = process_context(context, chromosome)
        results.extend(r)

    logging.info("saving")
    Utilities.ensure_requisite_folders(args.output)

    results = ECAVIAR.results_to_data_frame(results)
    compression = "gzip" if ".gz" in args.output else None
    results.to_csv(args.output, index=False, sep="\t", compression=compression)
    logging.info("Ran.")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Process eCAVIAR",
                                     epilog=
"""Watch out for the intermediate folder.
Will be used with extreme prejudice.""")

    parser.add_argument("--eqtl_path", help="path to eqtl file")
    parser.add_argument("--eqtl_reference_file", help="File with eQTL genotype", default=None)
    parser.add_argument("--gwas_path", help="path to GWAS")
    parser.add_argument("--gwas_reference_folder", help="Path to reference panel data (PrediXcan format)",
        default="data/TGF_EUR_CAVIAR")
    parser.add_argument("--gwas_reference_pattern", help="Pattern to select dosage file (and extract chromosome)",
        default="1000GP_Phase3_chr([0-9]+).dosage.gz")
    parser.add_argument("--ecaviar_path", help="Path to eCAVIAR executable", default=None)
    parser.add_argument("--intermediate", help="working path with scratch data", default="intermediate")
    parser.add_argument("--output", help="where to save results")

    parser.add_argument("--verbosity",
                        help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything",
                        default = "10")

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)