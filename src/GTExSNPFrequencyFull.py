#! /usr/bin/env python
import logging
import gzip

from gene_tools import Logging
from gene_tools import Utilities
from gene_tools.GTEx import GTExSNPMatrix

def build_frequency(gtex_geno_path, output_path):
    Utilities.ensure_requisite_folders(output_path)
    logging.info("Starting process")

    with gzip.open(output_path, "w") as output_file:
        output_file.write("variant\tfrequency\n")
        for i, entry in enumerate(GTExSNPMatrix.frequencies_iterator(gtex_geno_path)):
            variant = entry[0]
            frequency = entry[1]
            line = "%s\t%f\n" % (variant, frequency)
            output_file.write(line)

    logging.info("Ran")

def run(args):
    build_frequency(args.gtex_geno_path, args.output_path)

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Build snp frequency")

    parser.add_argument("--gtex_geno_path", help="path to GTEx snp dosage", default=None)
    parser.add_argument("--output_path", help="where to save the files", default=None)

    parser.add_argument("--verbosity",
                        help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything",
                        default = "10")

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)