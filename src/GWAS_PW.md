# GWAS-PW

`gwas-pw` estimates Bayes factor for "regions" of the chromosome,
estimating priors for SNP causality in a genome-wide fashion ($\\pi_i$).
This is different to the locus-wide approach of COLOC.

For `variance in effect size`, I assume `slope_se**2`