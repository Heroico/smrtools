#!/usr/bin/env python
import gzip
import pandas

P="/home/numa/Documents/Projects/data/GTEx/v6p/eqtl_all/Adipose_Subcutaneous_Analysis.v6p.all_snpgene_pairs.txt.gz"
d={}
with gzip.open(P) as f:
    for line in f:
        comps=line.strip().split()
        gene = comps[0]
        d[gene] = d.get(gene,0)+1
d = pandas.DataFrame(data={"gene":d.keys(), "count":d.values()})
d.to_csv("Adipose_count.txt", index=False,sep="\t")
