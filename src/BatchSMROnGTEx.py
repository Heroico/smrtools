#! /usr/bin/env python
import logging
import os

from gene_tools import Logging
from gene_tools import Utilities
from gene_tools.jobs import PBS
from gene_tools.misc import PhenoTissueGene

SIGNIF=".v6p.signif_snpgene_pairs.txt.gz"
import re
marr = re.compile(".ma$")

def build_command(args, gwas_file, eqtl_file):
    gwas_name = Utilities.pheno_name_heuristic(gwas_file)
    eqtl_name = Utilities.tissue_name_heuristic(eqtl_file)
    name = "{}_eQTL_{}".format(gwas_name, eqtl_name)
    job_name = "job_smr_on_gtex_"+name
    job_mem = "12gb"
    walltime = args.expected_walltime if args.expected_walltime else "12:00:00"
    command = PBS.job_header(job_name, log_folder=args.logs_folder,
                job_mem=job_mem, walltime=walltime, email=args.email)

    output = os.path.join(args.results_path, name)
    working = os.path.join(args.working_folder, name)

    if args.conda_env_name:
        command += "\nsource activate {}\n\n".format(args.conda_env_name)

    command += "python {} \\\n".format(os.path.join(args.smrtools_path, "SMROnGTEx.py"))
    command += "--index_filter {} \\\n".format(args.index_filter)
    command += "--reference {} \\\n".format(args.reference)
    command += "--gwas_path {} \\\n".format(os.path.join(args.gwas_folder, gwas_file))
    command += "--gtex_eqtl_path {} \\\n".format(os.path.join(args.gtex_eqtl_folder, eqtl_file))
    command += "--gtex_frequency_path {} \\\n".format(os.path.join(args.gtex_frequency_folder, eqtl_name+".txt.gz"))
    command += "--gtex_snp_path {} \\\n".format(args.gtex_snp_path)
    command += "--gencode_path {} \\\n".format(args.gencode_path)
    command += "--working_folder {} \\\n".format(working)
    command += "--smr_command {} \\\n".format(args.smr_command)

    if args.smr_log_folder:
        smr_log = os.path.join(args.smr_log_folder, name)
        command += "--smr_log_prefix {} \\\n".format(smr_log)

    command += "--output_prefix {}".format(output)
    return job_name, command, output

def get_besd_paths(args):
    j = os.path.join
    besd = sorted(os.listdir(args.besd_folder))
    besd = [j(j(args.besd_folder,x),x) for x in besd]
    return besd

#TODO: move into shared code, ythis is copy pasted from coloc full
def _should_skip_pheno(pheno, pheno_white_list,  index_filter):
    if pheno_white_list and not pheno in pheno_white_list:
        logging.info("%s not in whitelist, skipping", pheno)
        return True

    if index_filter is not None:
        eqtls = PhenoTissueGene.interrogate(index_filter, pheno)
        if len(eqtls) == 0:
            logging.info("%s has no significant results, skipping", pheno)
            return True

    return False

#TODO: move into shared code, ythis is copy pasted from coloc full
def _should_skip(pheno, eqtl, tissue_white_list, index_filter):
    if tissue_white_list and not eqtl in tissue_white_list:
        logging.info("%s not in whitelist, skipping", eqtl)
        return True

    if index_filter is not None:
        genes = PhenoTissueGene.interrogate(index_filter, pheno, eqtl)
        if len(genes) == 0:
            logging.info("(%s/%s) has no signicant results, skipping", pheno, eqtl)
            return True

    return False

def run(args):
    if not os.path.exists(args.jobs_folder): os.makedirs(args.jobs_folder)
    if not os.path.exists(args.logs_folder): os.makedirs(args.logs_folder)
    if not os.path.exists(args.working_folder): os.makedirs(args.working_folder)
    if not os.path.exists(args.results_path): os.makedirs(args.results_path)
    if args.smr_log_folder and not os.path.exists(args.smr_log_folder): os.makedirs(args.smr_log_folder)

    gwas_files = sorted(os.listdir(args.gwas_folder))
    eqtl_files = sorted(os.listdir(args.gtex_eqtl_folder))

    index_filter = PhenoTissueGene.load_pheno_tissue_gene(args.index_filter)

    tissue_white_list = set(args.tissue_white_list) if args.tissue_white_list else None
    pheno_white_list = set(args.pheno_white_list) if args.pheno_white_list else None

    for g in gwas_files:
        if _should_skip_pheno(g, pheno_white_list, index_filter):
            continue

        for e in eqtl_files:
            if tissue_white_list and not e in tissue_white_list:
                logging.info("%s not in whitelist, skipping", e)
                continue

            if _should_skip(g, e, tissue_white_list, index_filter):
                continue

            logging.info("Processing %s/%s", g, e)
            job_name, command, output = build_command(args, g, e)
            if os.path.exists(output+".smr"):
                logging.info("%s already exists, delete it if you want it done again", output)
                continue
            job_id = PBS.submit_command(args.jobs_folder, job_name, command, fake=args.fake_submission)
            msg="Submitted {}|{}".format(str(job_id) if job_id else "error for %s" % (job_name), job_name)
            logging.info(msg)

    logging.info("Batched!")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Convert GTEx eQTL to ESD")
    parser.add_argument("--jobs_folder", help="Where the submission jobs will be saved", default="jobs")
    parser.add_argument("--logs_folder", help="Where the job logs will be saved", default="logs")
    parser.add_argument("--email", help="email to receive job notifications")
    parser.add_argument("--expected_walltime", help="expected running length of each job")
    parser.add_argument("--conda_env_name", help="Name of conda env, optional")
    parser.add_argument("--index_filter", help="File with pheno,tissue,gene filter")
    parser.add_argument("--reference", help="Path where the reference pnale is (plink format)")
    parser.add_argument("--smr_command", help="what smr executable to use")
    parser.add_argument("--smrtools_path", help= "Path to smr tools")
    parser.add_argument("--working_folder", help="Path where intermediate stuff will be kept")
    parser.add_argument("--gwas_folder", help="Path where the GWAS in COJO format are")
    parser.add_argument("--gtex_eqtl_folder", help="Path where the EQTL BESD studies are")
    parser.add_argument("--gtex_frequency_folder", help="path to gtex frequency")
    parser.add_argument("--gtex_snp_path", help="path to GTEx snp file")
    parser.add_argument("--gencode_path", help="path to Gencode file")
    parser.add_argument("--results_path", help="Path where the results will be saved")
    parser.add_argument("--verbosity", help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything", default = "10")
    parser.add_argument("--fake_submission", help="wether to submit or not", action="store_true", default=False)
    parser.add_argument("--smr_log_folder", help="Optional folder to keep SMR output in")
    parser.add_argument("--log_file", help="name of file wher eyou would like a log")
    parser.add_argument('--pheno_white_list', type=str, nargs='+', help='If given, will only run phenos matching this names', default=None)
    parser.add_argument('--tissue_white_list', type=str, nargs='+', help='If given, will only run phenos matching this names', default=None)

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity), log_file=args.log_file)

    run(args)