#! /usr/bin/env python

import os
import logging
import pandas
import numpy

from gene_tools import Logging
from gene_tools.jobs import PBS

def get_gwas_sample_size(gwas_data_path):
    p = pandas.read_table(gwas_data_path)
    p = p.rename(columns={"Tag":"name", "Sample_Size":"sample_size"})
    p = p[["name", "sample_size"]]
    results = {}
    for gwas, sample_size in p.values:
        if gwas == "DIAGRAM_T2D_TRANS_ETHNIC": continue
        results[gwas] = sample_size
    return results

def get_eqtl_sample_size(eqtl_data_path):
    p = pandas.read_table(eqtl_data_path)
    results = {}
    for eqtl, sample_size in p.values:
        if "_Analysis" in eqtl: eqtl = eqtl.split("_Analysis")[0]
        results[eqtl] = sample_size
    return  results

def build_command(args, gwas_file, eqtl_file, gwas_sample_sizes, eqtl_sample_sizes):
    gwas_path = os.path.join(args.gwas_folder, gwas_file)
    gwas_key = gwas_file.split(".ma")[0]
    if not gwas_key in gwas_sample_sizes:
        return None,None
    gwas_sample_size=gwas_sample_sizes[gwas_key]
    if not numpy.isfinite(gwas_sample_size):
        return None, None
    gwas_sample_size = int(float(gwas_sample_sizes[gwas_key]))

    eqtl_path = os.path.join(args.eqtl_folder, eqtl_file)
    eqtl_key = eqtl_file.split(".txt.gz")[0]
    eqtl_sample_size = int(float(eqtl_sample_sizes[eqtl_key]))

    name = "_eQTL_".join([gwas_key, eqtl_key])
    output = os.path.join(args.output_folder, name + ".txt")

    job_name = "job_coloc_" + name
    job_mem = "4gb"
    command = PBS.job_header(job_name, module_clause="module load python/2.7.9", job_mem=job_mem)
    command += "python {} \\\n".format(os.path.join(args.smrtools_path, "Coloc.py"))
    command += "--gwas_path {} \\\n".format(gwas_path)
    command += "--gwas_N {} \\\n".format(gwas_sample_size)
    command += "--eqtl_path {} \\\n".format(eqtl_path)
    command += "--eqtl_N {} \\\n".format(eqtl_sample_size)
    command += "--output {} \\\n".format(output)
    command += "--verbosity {}".format(args.verbosity)

    if args.serialize_local:
        log_path = os.path.join(args.logs_folder, name) + ".log"
        command += " > {}".format(log_path)

    return job_name, command

def run(args):
    if not os.path.exists(args.output_folder): os.makedirs(args.output_folder)
    if not os.path.exists(args.jobs_folder): os.makedirs(args.jobs_folder)
    if not os.path.exists(args.logs_folder): os.makedirs(args.logs_folder)

    gwas_sample_sizes = get_gwas_sample_size(args.gwas_data_file)
    eqtl_sample_sizes = get_eqtl_sample_size(args.eqtl_samples_file)
    gwas_files = sorted(os.listdir(args.gwas_folder))
    eqtl_files = sorted(os.listdir(args.eqtl_folder))

    for g in gwas_files:
        for e in eqtl_files:
            logging.info("Processing %s/%s", g, e)
            job_name, command = build_command(args, g, e, gwas_sample_sizes, eqtl_sample_sizes)
            if not job_name:
                logging.info("No job for %s/%s",g,e)
                continue

            job_id = PBS.submit_command("jobs", job_name, command, fake=args.fake_submission, serialize_local=args.serialize_local)
            msg = "Submitted {}|{}".format(str(job_id) if job_id else "error for %s" % (job_name), job_name)
            logging.info(msg)

    logging.info("Batched!")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Run COLOC analysis between GWAS and GTEx eQTL")
    parser.add_argument("--jobs_folder", help="Where the submission jobs will be saved", default="jobs")
    parser.add_argument("--logs_folder", help="Where the job logs will be saved", default="logs")
    parser.add_argument("--smrtools_path", help= "Path to smr tools")
    parser.add_argument("--eqtl_folder", help="path to eqtl folder")
    parser.add_argument("--eqtl_samples_file", help="Number of samples in eQTL")
    parser.add_argument("--gwas_folder", help="path to GWAS")
    parser.add_argument("--gwas_data_file", help="Number of samples in GWAS")
    parser.add_argument("--output_folder", help="path where SMR ESD will be built")
    parser.add_argument("--verbosity", help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything", default = "10")
    parser.add_argument("--fake_submission", help="wether to submit or not", action="store_true", default=False)
    parser.add_argument("--serialize_local", help="Just run locally", action="store_true", default=False)

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)