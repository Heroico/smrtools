#! /usr/bin/env python
"""
Quick & Ugly script to check job submission status, copy pasted for self completeness
"""
import logging
import os
import sys
import time
import re
from subprocess import Popen, PIPE
import pandas

def configure_logging(level=5, target=sys.stderr):
    logger = logging.getLogger()
    logger.setLevel(level)

    # create console handler and set level to info
    handler = logging.StreamHandler(target)
    handler.setLevel(level)
    formatter = logging.Formatter("%(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

def submit_command(path):
    retry = 0

    command = ["qsub",path]
    def submit():
        logging.info("Submitting Command: %s", " ".join(command))
        proc = Popen(command,stdout=PIPE, stderr=PIPE)
        out, err = proc.communicate()
        exitcode = proc.returncode
        return exitcode, out, err

    exitcode, out, err = submit()
    while exitcode != 0 and retry < 10:
        logging.info("retry %s %i-th attempt", path, retry)
        exitcode, out, err = submit()
        retry += 1
        time.sleep(0.1)
    time.sleep(0.1)
    return out.strip() if exitcode==0 else None

def get_job_targets(path):
    jobs_regexp = re.compile('job_smr_(.*).sh')
    jobs = sorted(os.listdir(path))
    job_names = [split[1] for split in [os.path.split(x) for x in jobs]]
    job_names = [jobs_regexp.search(x).group(1) for x in job_names]
    jobs = [os.path.join(path,x) for x in jobs]
    return job_names, jobs

def get_result_present(smr_results_path, targets):
    smr = re.compile(".smr$")
    expected_results = [x+".smr" if not smr.match(x) else x for x in targets]
    logging.info("Found %d expected results", len(targets))
    results = set(os.listdir(smr_results_path))
    logging.info("Found %d results", len(results))
    present = [(x in results) for x in expected_results]
    logging.info("Present: %d", len(present))
    return present

def build_presence(job_path, smr_results_path):
    targets, jobs = get_job_targets(job_path)
    present = get_result_present(smr_results_path, targets)
    presence = {"target":targets, "present":present, "jobs":jobs}
    presence = pandas.DataFrame(presence)
    presence = presence.sort_values(by="target")
    presence = presence[["target", "present", "jobs"]]
    return presence

def resubmit(presence):
    for row in presence.values:
        if row[1] == True: continue
        path = row[2]
        submit_command(path)

# TODO: FIX!!!!! get_result_present is wrong.
def run(args):
    presence = build_presence(args.jobs_folder, args.smr_results_path)
    s = presence[["target","present"]]
    s.to_csv(args.output, index=False, sep="\t")

    if args.resubmit:
        resubmit(presence)

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Convert GTEx eQTL to ESD")
    parser.add_argument("--jobs_folder",
                        help="Where the submission jobs will be saved",
                        default="jobs")

    parser.add_argument("--logs_folder",
                        help="Where the job logs will be saved",
                        default="logs")

    parser.add_argument("--smr_results_path",
                        help= "Path to smr files",
                        default="results/smr")

    parser.add_argument("--output",
                        help="Path where the output will be saved",
                        default="job_log.txt")

    parser.add_argument("--verbosity",
                        help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything",
                        default = "10")

    parser.add_argument("--resubmit",
                    help="wether to submit missing jobs",
                    action="store_true",
                    default=False)

    args = parser.parse_args()

    configure_logging(int(args.verbosity))

    run(args)