#! /usr/bin/env python

import os
import stat
import logging
import pandas
import numpy
from gene_tools.jobs import SLURM

from gene_tools import Logging

def get_gwas_sample_size(gwas_data_path):
    p = pandas.read_table(gwas_data_path)
    p = p.rename(columns={"Tag":"name", "Sample_Size":"sample_size"})
    p = p[["name", "sample_size"]]
    results = {}
    for gwas, sample_size in p.values:
        if gwas == "DIAGRAM_T2D_TRANS_ETHNIC": continue
        results[gwas] = sample_size
    return results

def get_eqtl_sample_size(eqtl_data_path):
    p = pandas.read_table(eqtl_data_path)
    results = {}
    for eqtl, sample_size in p.values:
        if "_Analysis" in eqtl: eqtl = eqtl.split("_Analysis")[0]
        results[eqtl] = sample_size
    return  results

def build_command(args, gwas_file, eqtl_file, gwas_sample_sizes, eqtl_sample_sizes):
    gwas_path = os.path.join(args.gwas_folder, gwas_file)
    gwas_key = gwas_file.split(".ma")[0]
    if not gwas_key in gwas_sample_sizes:
        return None,None
    gwas_sample_size=gwas_sample_sizes[gwas_key]
    if not numpy.isfinite(gwas_sample_size):
        return None, None
    gwas_sample_size = int(float(gwas_sample_sizes[gwas_key]))

    eqtl_path = os.path.join(args.eqtl_folder, eqtl_file)
    eqtl_key = eqtl_file.split("_Analysis")[0]
    eqtl_sample_size = int(float(eqtl_sample_sizes[eqtl_key]))

    eqtl_frequency = [x for x in os.listdir(args.eqtl_frequency_folder) if eqtl_key in x][0]
    eqtl_frequency = os.path.join(args.eqtl_frequency_folder, eqtl_frequency)

    name = "_eQTL_".join([gwas_key, eqtl_key])
    output = os.path.join(args.output_folder, name + ".txt")
    log = os.path.join(_log_r_path(args), name + ".log")

    job_name = "job_coloc_full_" + name
    command = SLURM.job_header(job_name, logs_folder=_log_kk_path(args),
                    time="36:00:00", mem="12288") + "\n"
    #command += "module load R\n\n".format(args.conda_env_name)
    command += "source activate {}\n\n".format(args.conda_env_name)
    command += "python {} \\\n".format(os.path.join(args.smrtools_path, "ColocFull.py"))
    command += "--gtex_snp_path {} \\\n".format(args.gtex_snp_path)
    command += "--gwas_path {} \\\n".format(gwas_path)
    command += "--gwas_N {} \\\n".format(gwas_sample_size)
    command += "--eqtl_path {} \\\n".format(eqtl_path)
    command += "--eqtl_frequency {} \\\n".format(eqtl_frequency)
    command += "--eqtl_N {} \\\n".format(eqtl_sample_size)
    command += "--output {} \\\n".format(output)
    command += "--verbosity {}".format(args.verbosity)
    command += " >/dev/null 2>{} \n".format(log)

    return job_name, command

def _job_coloc_path(args):
    return os.path.join(args.jobs_folder, "coloc")

def _job_submit_path(args):
    return os.path.join(args.jobs_folder, "submit")

def _log_kk_path(args):
    return os.path.join(args.logs_folder, "kk")

def _log_r_path(args):
    return os.path.join(args.logs_folder, "r")


def _split(l, size):
    parts = []
    while len(l) > size:
        part = l[:size]
        parts.append(part)
        l = l[size:]
    parts.append(l)
    return parts

def run(args):
    if not os.path.exists(args.output_folder): os.makedirs(args.output_folder)
    if not os.path.exists(args.jobs_folder): os.makedirs(args.jobs_folder)
    if not os.path.exists(args.logs_folder): os.makedirs(args.logs_folder)
    # job_coloc = _job_coloc_path(args)
    # if not os.path.exists(job_coloc): os.makedirs(job_coloc)
    # job_submit = _job_submit_path(args)
    # if not os.path.exists(job_submit): os.makedirs(job_submit)
    log_kk = _log_kk_path(args)
    if not os.path.exists(log_kk): os.makedirs(log_kk)
    log_r = _log_r_path(args)
    if not os.path.exists(log_r): os.makedirs(log_r)

    gwas_sample_sizes = get_gwas_sample_size(args.gwas_data_file)
    eqtl_sample_sizes = get_eqtl_sample_size(args.eqtl_samples_file)
    gwas_files = sorted(os.listdir(args.gwas_folder))
    eqtl_files = sorted(os.listdir(args.eqtl_folder))

    pheno_white_list = set(args.pheno_white_list) if args.pheno_white_list else None

    for g in gwas_files:
        if pheno_white_list and not g in pheno_white_list:
            logging.info("%s not in whitelist, skipping", g)
            continue
        for e in eqtl_files:
            logging.info("Processing %s/%s", g, e)
            job_name, command = build_command(args, g, e, gwas_sample_sizes, eqtl_sample_sizes)
            if not job_name:
                logging.info("No job for %s/%s",g,e)
                continue
            job_id = SLURM.submit_command(args.jobs_folder, job_name, command,
                        fake=args.fake_submission, serialize_local=args.serialize_local)
            msg = "Submitted {}|{}".format(str(job_id) if job_id else "error for %s" % (job_name), job_name)
            logging.info(msg)


    logging.info("Batched!")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Run COLOC analysis between GWAS and GTEx eQTL")
    parser.add_argument("--jobs_folder", help="Where the submission jobs will be saved", default="jobs")
    parser.add_argument("--logs_folder", help="Where the job logs will be saved", default="logs")
    parser.add_argument("--smrtools_path", help= "Path to smr tools")
    parser.add_argument("--conda_env_name", help= "conda env name")
    parser.add_argument("--gtex_snp_path", help="path to GTEx snp file")
    parser.add_argument("--submission_size", help="How many jobs you wnat grouped when submitting", type=int, default=16)
    parser.add_argument("--eqtl_folder", help="path to eqtl file")
    parser.add_argument("--eqtl_frequency_folder", help="path to eqtl file")
    parser.add_argument("--eqtl_samples_file", help="Number of samples in eQTL")
    parser.add_argument("--gwas_folder", help="path to GWAS file")
    parser.add_argument("--gwas_data_file", help="Number of samples in GWAS")
    parser.add_argument("--output_folder", help="path where COLOC result will be built")
    parser.add_argument("--verbosity", help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything", default = "10")
    parser.add_argument("--fake_submission", help="wether to submit or not", action="store_true", default=False)
    parser.add_argument("--serialize_local", help="Just run locally", action="store_true", default=False)
    parser.add_argument('--pheno_white_list', type=str, nargs='+', help='If given, will only run phenos matching this names', default=None)

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)