

# Generalities

`Batch*py` are scripts that automate/submit/serialize the running of a family of jobs, such as preparing data for running SMR or COLOC.
`Batch*sh` are scripts that run the matching `Batch*py` file. the bash scripts are merely a wrapper around the python script and the environment data paths.


