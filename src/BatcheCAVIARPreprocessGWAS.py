#! /usr/bin/env python
import logging
import os

from gene_tools import Logging
from gene_tools.jobs import PBS
from gene_tools.jobs import KnownGWAS

SIGNIF=".v6p.signif_snpgene_pairs.txt.gz"

def build_command(args, job):
    name = job[KnownGWAS.NAME]
    job_name = "job_gwaste_"+ name
    job_mem = "8gb" if not KnownGWAS.JOB_MEM in job else job[KnownGWAS.JOB_MEM]
    command = PBS.job_header(job_name, log_folder=args.logs_folder,
                module_clause="module load python/2.7.9", job_mem=job_mem)
    command += "python {} \\\n".format(args.tool_path)
    command += "{} \\\n".format(job[KnownGWAS.BETA_PARAMETERS])
    command += "--gwas_file_path {} \\\n".format(os.path.join(args.gwas_folder, job[KnownGWAS.GWAS_PATH]))
    command +=  "--output_path {}".format(os.path.join(args.results_path, name+".txt.gz"))
    return job_name, command

def run(args):
    if not os.path.exists(args.jobs_folder): os.makedirs(args.jobs_folder)
    if not os.path.exists(args.logs_folder): os.makedirs(args.logs_folder)
    if not os.path.exists(args.results_path): os.makedirs(args.results_path)

    gwas_contents = sorted(os.listdir(args.gwas_folder))
    for folder in gwas_contents:
        if not folder in KnownGWAS.JOBS:
            logging.info("%s not present in jobs, skipping", folder)
            continue

        logging.info("Processing %s", folder)

        jobs = KnownGWAS.JOBS[folder]
        for job in jobs:
            name = job[KnownGWAS.NAME]
            if "_1KGf" in name:
                logging.log(9, "%s is 1kgf, skipping", name)
                continue
            # if not name in KnownGWAS.SANCTIONED:
            #     logging.log(9, "%s not quite there yet, skipping", name)
            #     continue
            job_name, command = build_command(args, job)
            job_id = PBS.submit_command(args.jobs_folder, job_name, command, fake=args.fake_submission ,serialize_local=args.serialize_local)
            msg = "Submitted {}|{}".format(str(job_id) if job_id else "error for %s" % (job_name), job_name)
            logging.info(msg)

    logging.info("Batched!")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Convert GTEx eQTL to ESD")
    parser.add_argument("--jobs_folder", help="Where the submission jobs will be saved", default="jobs")
    parser.add_argument("--logs_folder", help="Where the job logs will be saved", default="logs")
    parser.add_argument("--tool_path", help= "Path to script")
    parser.add_argument("--gwas_folder", help="Path where the Production GWAS are at")
    parser.add_argument("--results_path", help="Path where the converted GWAS will be saved")
    parser.add_argument("--verbosity", help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything", default = "10")
    parser.add_argument("--fake_submission", help="wether to submit or not", action="store_true", default=False)
    parser.add_argument("--serialize_local", help="Just run locally", action="store_true", default=False)

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)