#! /usr/bin/env python

import os
import logging
import pandas
import shutil
import numpy

from subprocess import Popen, PIPE

from gene_tools import Logging
from gene_tools import Utilities
from gene_tools.misc import KeyedDataSource
from gene_tools.misc import DataFrameStreamer
from gene_tools.misc import PhenoTissueGene
from timeit import default_timer as timer

from gene_tools.misc.Wrangling import align_data_to_alleles

def prepare_data(eqtl_data, gwas, gtex_snp):
    eqtl_data = eqtl_data.rename(columns={"slope":"beta", "slope_se":"se"})
    eqtl_data["comps"] = eqtl_data.variant_id.str.split("_")
    eqtl_data["CHR"] = "chr"+eqtl_data.comps.str.get(0)
    eqtl_data["POS"] = eqtl_data.comps.str.get(1)
    e_s = KeyedDataSource.to_data_frame(gtex_snp, set(eqtl_data.variant_id), "variant_id", "rsid")
    eqtl_data = pandas.merge(eqtl_data, e_s, on="variant_id", how="inner")
    if not len(eqtl_data):
        return pandas.DataFrame()
    k = eqtl_data.variant_id.str.split("_")
    eqtl_data["non_effect_allele"] = k.str.get(2)
    eqtl_data["effect_allele"] = k.str.get(3)
    data = align_data_to_alleles(gwas, eqtl_data, left_on="rsid", right_on="rsid", flips=["g_beta"], how="right")

    #It would seem gwas-pw does not use
    data = data.dropna()

    data["Z_EQTL"] = data.beta / data.se
    data["V_EQTL"] = data.se ** 2
    data["Z_GWAS"] = data.g_beta / data.g_se
    data["V_GWAS"] = data.g_se ** 2
    data = data.rename(columns={"rsid":"SNPID"})
    data = data[["SNPID", "CHR", "POS", "Z_EQTL", "V_EQTL", "Z_GWAS", "V_GWAS"]]
    data.POS = data.POS.astype(int)
    data = data.sort_values(by="POS")
    return data

def gwas_pw_command(args, folder, input_file):
    output_stem = os.path.join(folder, "results")
    command = "{} \\\n".format(args.gwas_pw_path)
    command += "-i {} \\\n".format(input_file)
    command += "-bed {} \\\n".format(args.gwas_pw_bed_path)
    command += "-phenos EQTL GWAS \\\n"
    command += "-o {} ".format(output_stem)
    return output_stem, command

def run_gwas_pw_command(run_sh):
    p = Popen(["bash", run_sh], stdin=PIPE, stdout=PIPE, stderr=PIPE)
    output, err = p.communicate()

# gene, P0, P1, P2, P3, P4
def extract_result(gene, output_stem, data):
    result = pandas.read_table(output_stem+".segbfs.gz", sep="\s+")
    regions = result.shape[0]
    result = result.sort_values(by="PPA_4", ascending=False).head(1)[["PPA_1", "PPA_2", "PPA_3", "PPA_4"]].values[0]
    p_0 = 1-float(result[0])-float(result[1])-float(result[2])-float(result[3])
    result = (gene, p_0, result[0], result[1], result[2], result[3], data.shape[0], regions, numpy.nan)
    return result

def empty_result(gene, error=numpy.nan):
    return (gene, numpy.nan, numpy.nan, numpy.nan, numpy.nan, numpy.nan, numpy.nan, numpy.nan, error)

def run_gwas_pw(args, gene, data):
    if data.shape[0] == 0:
        return empty_result(gene)

    folder = os.path.join(args.working_folder, gene)

    if os.path.exists(folder):
        raise RuntimeError("gwas-pw Working folder already exists!")
    else:
        os.makedirs(folder)
    input_file= os.path.join(folder, "gwas_pw_input.txt.gz")
    data.to_csv(input_file, index=False, sep=" ", compression="gzip", na_rep="NA")
    output_stem, command = gwas_pw_command(args, folder, input_file)
    run_sh = os.path.join(folder, "run.sh")
    with open(run_sh, "w") as file:
        file.write(command)
    run_gwas_pw_command(run_sh)
    result = extract_result(gene, output_stem, data)
    shutil.rmtree(folder)
    return  result

def process(args, eqtl_data, gwas, gtex_snp):
    data = prepare_data(eqtl_data, gwas, gtex_snp)
    gene = eqtl_data.gene_id.values[0]
    result = run_gwas_pw(args, gene, data)
    return result

########################################################################################################################

def run(args):
    if os.path.exists(args.output):
        logging.info("%s already exists. Delete it or move it if you want it done again", args.output)
        return

    if not args.working_folder:
        logging.info("Must provide working folder")
        return

    if os.path.exists(args.working_folder):
        logging.info("Must provide a working folder, exclusively for this run.")
        return

    start = timer()

    valid_genes=None
    if args.index_filter:
        logging.info("Loading index filter")
        index_filter = PhenoTissueGene.load_pheno_tissue_gene(args.index_filter)

        valid_genes = PhenoTissueGene.interrogate(index_filter, args.gwas_path, args.eqtl_path)
        if len(valid_genes) == 0:
            logging.info("Phenotype not in filter. Aborting")
            return
        logging.info("%d valid genes in index_filter", len(valid_genes))

    logging.info("Loading gwas")
    gwas = pandas.read_table(args.gwas_path)
    gwas = gwas.rename(columns={"A1":"effect_allele", "A2":"non_effect_allele", "b":"g_beta", "se":"g_se", "SNP":"rsid", "freq":"g_frequency"})

    logging.info("Loading GTEx snp")
    gtex_snp = KeyedDataSource.load_data(args.gtex_snp_path, "VariantID", "RS_ID_dbSNP142_CHG37p13", numeric=False)

    logging.info("Processing")
    results = []

    found_variants = set()
    reporter = Utilities.PercentReporter(logging.INFO, len(gtex_snp), increment=1)
    reporter.update(0, "%d %% of snps from gwas found so far")

    for i,d in enumerate(DataFrameStreamer.data_frame_streamer(args.eqtl_path, "gene_id", valid_genes, ".")):
        try:
            logging.log(7, "Processing gene %s", d.gene_id.values[0])
            found_variants.update(set(d.variant_id).intersection(gtex_snp.keys()))
            reporter.update(len(found_variants))

            result = process(args, d, gwas, gtex_snp)
            results.append(result)

        except Exception as e:
            g = None
            try:
                g = d.gene_id.values[0]
            except: pass
            logging.info("Problem: %s\n %s", g, str(e))
            results.append(empty_result(g, str(e)))

    reporter.update(len(found_variants), force=True)

    Utilities.ensure_requisite_folders(args.output)
    results = results if len(results) else [[], [], [], [], [], [], []]
    results = Utilities.to_dataframe(results, ["gene_id", "P_H0", "P_H1", "P_H2", "P_H3", "P_H4", "n_snps", "n_regions"], fill_na="NA")
    compression = "gzip" if ".gz" in args.output else None
    results.to_csv(args.output, index=False, sep="\t", compression=compression)

    logging.info("Cleaning folder")
    shutil.rmtree(args.working_folder)

    end = timer()
    logging.info("Successfully ran gwas-pw in %s seconds" % (str(end - start)))

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Run COLOC analysis between GWAS and GTEx eQTL")

    parser.add_argument("--gtex_snp_path", help="path to GTEx snp file")
    parser.add_argument("--eqtl_path", help="path to eqtl file")
    parser.add_argument("--gwas_path", help="path to GWAS file")
    parser.add_argument("--output", help="path where COLOC result will be built")
    parser.add_argument("--index_filter", help="Optional")
    parser.add_argument("--working_folder", help="Where the gwas-pw will work temporarily")
    parser.add_argument("--gwas_pw_path", help="Path to gwas-pw executable")
    parser.add_argument("--gwas_pw_bed_path", help="Path to bed file with regions")


    parser.add_argument("--verbosity",
                        help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything",
                        default = "10")

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)