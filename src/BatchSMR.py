#! /usr/bin/env python
import logging
import os

from gene_tools import Logging
from gene_tools.jobs import PBS
from gene_tools.jobs import KnownSMR

SIGNIF=".v6p.signif_snpgene_pairs.txt.gz"
import re
marr = re.compile(".ma$")

def build_command(args, file, besd_path):
    besd_folder, besd_name = os.path.split(besd_path)
    name = marr.split(file)[0]
    name = "{}_eQTL_{}".format(name, besd_name)
    job_name = "job_smr_"+name
    job_mem = "16gb"
    command = PBS.job_header(job_name, log_folder=args.logs_folder,
                job_mem=job_mem, walltime="12:00:00")
    output = os.path.join(args.results_path, name)
    command += "{} \\\n".format(args.smr_command)
    command += "--gwas-summary {} \\\n".format(os.path.join(args.cojo_folder, file))
    command += "--bfile {} \\\n".format(args.reference)
    command += "--beqtl-summary {} \\\n".format(besd_path)
    command +=  "--out {}".format(output)
    return job_name, command, output

def get_besd_paths(args):
    j = os.path.join
    besd = sorted(os.listdir(args.besd_folder))
    besd = [j(j(args.besd_folder,x),x) for x in besd]
    return besd

def run(args):
    if not os.path.exists(args.jobs_folder): os.makedirs(args.jobs_folder)
    if not os.path.exists(args.logs_folder): os.makedirs(args.logs_folder)
    if not os.path.exists(args.results_path): os.makedirs(args.results_path)

    cojo_contents = sorted(os.listdir(args.cojo_folder))
    besd_paths = get_besd_paths(args)

    for file in cojo_contents:
        logging.info("Processing %s", file)

        for besd_path in besd_paths:
            job_name, command, output = build_command(args, file, besd_path)
            if os.path.exists(output+".smr"):
                logging.info("%s already exists, delete it if you want it done again", output)
                continue
            job_id = PBS.submit_command(args.jobs_folder, job_name, command, fake=args.fake_submission)
            msg="Submitted {}|{}".format(str(job_id) if job_id else "error for %s" % (job_name), job_name)
            logging.info(msg)

    logging.info("Batched!")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Convert GTEx eQTL to ESD")
    parser.add_argument("--jobs_folder", help="Where the submission jobs will be saved", default="jobs")
    parser.add_argument("--logs_folder", help="Where the job logs will be saved", default="logs")
    parser.add_argument("--smr_command", help="what smr executable to use")
    parser.add_argument("--smrtools_path", help= "Path to smr tools")
    parser.add_argument("--cojo_folder", help="Path where the GWAS in COJO format are")
    parser.add_argument("--reference", help="Path where the reference pnale is (plink format)")
    parser.add_argument("--besd_folder", help="Path where the EQTL BESD studies are")
    parser.add_argument("--results_path", help="Path where the results will be saved")
    parser.add_argument("--verbosity", help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything", default = "10")
    parser.add_argument("--fake_submission", help="wether to submit or not", action="store_true", default=False)
    parser.add_argument("--log_file", help="name of file wher eyou would like a log")

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity), log_file=args.log_file)

    run(args)