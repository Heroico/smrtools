#! /usr/bin/env python

import os
import logging

from gene_tools import Logging

from gene_tools.jobs import PBS

def build_command(args, input_file):
    name = input_file.split("_Analysis")[0]
    job_name = "job_eqtlf_"+name
    job_mem = "2gb"

    output = os.path.join(args.output_folder, name)+".txt.gz"

    command = PBS.job_header(job_name, log_folder=args.logs_folder,
                module_clause="module load python/2.7.9", job_mem=job_mem, walltime="8:00:00")
    command += "{} \\\n".format(args.script_path)
    command += "--gtex_geno_path {} \\\n".format(os.path.join(args.gtex_geno_folder, input_file))
    command += "--output_path {} \\\n".format(output)
    command += "--verbosity {}".format(args.verbosity)
    return job_name, command

def run(args):
    if not os.path.exists(args.jobs_folder): os.makedirs(args.jobs_folder)
    if not os.path.exists(args.logs_folder): os.makedirs(args.logs_folder)
    if not os.path.exists(args.output_folder): os.makedirs(args.output_folder)

    eqtls = sorted(os.listdir(args.gtex_geno_folder))
    eqtls = [x for x in eqtls if "_Analysis" in x]
    for eqtl in eqtls:
        job_name,command = build_command(args, eqtl)
        job_id = PBS.submit_command(args.jobs_folder, job_name, command, fake=args.fake_submission,serialize_local=args.serialize_local)
        msg = "Submitted {}|{}".format(str(job_id) if job_id else "error for %s" % (job_name), job_name)
        logging.info(msg)

    logging.info("Batched!")


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Convert GTEx eQTL to ESD")

    parser.add_argument("--jobs_folder", help="Where the submission jobs will be saved", default="jobs")
    parser.add_argument("--logs_folder", help="Where the job logs will be saved", default="logs")
    parser.add_argument("--gtex_geno_folder", help="path to GTEx eqtl file", default=None)
    parser.add_argument("--output_folder", help="path where SMR ESD will be built", default=None)
    parser.add_argument("--script_path", help="Path to smr tools")
    parser.add_argument("--fake_submission", help="wether to submit or not", action="store_true", default=False)
    parser.add_argument("--serialize_local", help="Override any submission behaviour and just run serially.", action="store_true", default=False)
    parser.add_argument("--verbosity", help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything", default = "10")

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)