import os
from gene_tools.jobs import SLURM

def _job_coloc_path(args):
    return os.path.join(args.jobs_folder, "coloc")

def _job_submit_path(args):
    return os.path.join(args.jobs_folder, "submit")
def build_submission(args, jobs, i):
    job_coloc = _job_coloc_path(args)
    jobs_paths = [os.path.join(job_coloc,x) for x in jobs]

    submission_name = "job_submit_coloc_{}".format(i)

    header = SLURM.job_header(submission_name, logs_folder=args.logs_folder,
                time="12:00:00", mem="4096", ntasks=str(args.submission_size))
    submission = header + "\n"
    submission += "ulimit -u 10000 \n"

    submission += "module load parallel\n\n"

    submission += 'srun="srun --exclusive -N1 -n1"\n\n'
    submission += \
        'parallel="parallel --delay .2 -j {0} --joblog job_coloc_{1}.log --resume"\n\n'\
            .format(args.submission_size, i)

    jobs_to_submit = " \\\n".join(jobs_paths)
    submission += '$parallel -n0 "$srun bash {{1}}" ::: \\\n{}'.format(jobs_to_submit)

    return submission_name, submission

def build_submission_2(args, jobs, i):
    job_coloc = _job_coloc_path(args)
    jobs_paths = [os.path.join(job_coloc,x) for x in jobs]

    submission_name = "job_submit_coloc_{}".format(i)

    header = SLURM.job_header(submission_name, logs_folder=args.logs_folder,
                time="12:00:00", mem=4096, ntasks=str(args.submission_size))
    submission = header + "\n"
    submission += "ulimit -u 10000 \n"


    submission += "module load parallel\n\n"
    submission += 'srun="srun --exclusive -N1 -n1"\n'
    submission += 'parallel="parallel --delay .2 -j {} --joblog p_c_{}.log --resume"\n'.format(args.submission_size, i)
    jobs_to_submit = " ".join(jobs_paths)
    submission += '$parallel "$srun bash {{}}" ::: {}\n'.format(jobs_to_submit)

    return submission_name, submission