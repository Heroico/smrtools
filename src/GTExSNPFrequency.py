#! /usr/bin/env python
import logging

from gene_tools import Logging
from gene_tools.GTEx import GTExEQTLsignif
from gene_tools.GTEx import GTExSNPMatrix
from gene_tools import Utilities

def build_frequency(gtex_eqtl_path, gtex_geno_path, output_path):
    logging.info("Loading variants")
    variants = GTExEQTLsignif.variants(gtex_eqtl_path)

    logging.info("Loading frequencies")
    freq = GTExSNPMatrix.fequencies(gtex_geno_path,variants)

    logging.info("Saving")
    Utilities.ensure_requisite_folders(output_path)
    freq.to_csv(output_path, index=False, sep="\t")

    logging.info("Great success!")

#TODO: Switch to new code. Because.
def run(args):
    build_frequency(args.gtex_eqtl_path, args.gtex_geno_path, args.output_path)

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Build snp frequency")

    parser.add_argument("--gtex_eqtl_path",
                        help="path to GTEx eqtl file",
                        default="data/GTEx_Analysis_v6p_eQTL/Adipose_Subcutaneous_Analysis.v6p.signif_snpgene_pairs.txt.gz")

    parser.add_argument("--gtex_geno_path",
                        help="path to GTEx snp file",
                        default="data/GTEx_Analysis_v6p_eQTL/Adipose_Subcutaneous_Analysis.v6p.txt.gz")

    parser.add_argument("--output_path",
                        help="path to Gencode file",
                        default="results/Adipose_Subcutaneous_Analysis.freq.txt")

    parser.add_argument("--verbosity",
                        help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything",
                        default = "10")

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)