#! /usr/bin/env python

import os
import logging
import gzip
from gene_tools.misc import PhenoTissueGene
from gene_tools.misc import DataFrameStreamer
from gene_tools import Utilities

from gene_tools import Logging


def run(args):
    if os.path.exists(args.output):
        logging.info("%s already exists, delte/move it if you want it done again")
        return

    Utilities.ensure_requisite_folders(args.output)

    index_filter = PhenoTissueGene.load_pheno_tissue_gene(args.index_filter)
    valid_genes = PhenoTissueGene.interrogate(index_filter, args.pheno_filter, args.tissue_filter)

    with gzip.open(args.output, "a") as file:
        headerYet=False
        for i, d in enumerate(DataFrameStreamer.data_frame_streamer(args.eqtl_file, "gene_id", valid_genes, ".")):
            if not headerYet:
                d.to_csv(file, index=False, sep="\t")
                headerYet = True
                continue

            d.to_csv(file, index=False, sep="\t", header=False)

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Run COLOC analysis between GWAS and GTEx eQTL")
    parser.add_argument("--eqtl_file", help="path to eqtl file")
    parser.add_argument("--pheno_filter", help="Optional")
    parser.add_argument("--tissue_filter", help="Optional")
    parser.add_argument("--output", help="path where COLOC result will be built")
    parser.add_argument("--index_filter", help="Optional")
    parser.add_argument("--verbosity",
                        help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything",
                        default = "10")
    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)