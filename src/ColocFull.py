#! /usr/bin/env python

import os
import logging
import pandas


from gene_tools import Logging
from gene_tools import Utilities
from gene_tools.misc import KeyedDataSource
from gene_tools.misc import DataFrameStreamer
from gene_tools.misc import PhenoTissueGene
from timeit import default_timer as timer

from gene_tools.misc.Wrangling import align_data_to_alleles
from Coloc import helper, _coloc

def prepare_data(eqtl_data, gwas, eqtl_frequency, gtex_snp):
    eqtl_data = eqtl_data.rename(columns={"slope":"beta", "slope_se":"se"})
    e_f = KeyedDataSource.to_data_frame(eqtl_frequency, set(eqtl_data.variant_id), "variant_id", "frequency", "ignore")
    e_s = KeyedDataSource.to_data_frame(gtex_snp, set(eqtl_data.variant_id), "variant_id", "rsid")
    eqtl_data = pandas.merge(eqtl_data, e_s, on="variant_id", how="inner")
    eqtl_data = pandas.merge(eqtl_data, e_f, on="variant_id", how="inner")
    if not len(eqtl_data):
        return pandas.DataFrame()
    k = eqtl_data.variant_id.str.split("_")
    eqtl_data["non_effect_allele"] = k.str.get(2)
    eqtl_data["effect_allele"] = k.str.get(3)
    aligned = align_data_to_alleles(gwas, eqtl_data, left_on="rsid", right_on="rsid", flips=["g_beta"])
    aligned = aligned[["gene_id", "rsid", "effect_allele", "non_effect_allele", "g_frequency", "g_beta", "g_se", "frequency", "beta","se"]]
    return aligned

def process(eqtl_data, gwas, eqtl_frequency, gtex_snp, e_N, g_N):
    aligned = prepare_data(eqtl_data, gwas, eqtl_frequency, gtex_snp)
    aligned = aligned.dropna()
    gene = eqtl_data.gene_id.values[0]
    if len(aligned) == 0:
        r = _coloc(None, g_N=g_N, e_N=e_N)
        r = (gene,) + r
        return r

    data = helper(aligned)
    d = data[gene]
    #TODO: coloc sends to stdout. Should we add a printout of the gene?
    #print gene
    r = _coloc(d, g_N=g_N, e_N=e_N)
    r = (gene,) + r
    return r

def run(args):
    if not args.eqtl_N:
        logging.info("Must provide sample size for eqtl")
        return

    if not args.gwas_N:
        logging.info("Must provide sample size for GWAs")
        return

    if os.path.exists(args.output):
        logging.info("%s already exists. Delete it or move it if you want it done again", args.output)
        return

    start = timer()

    valid_genes=None
    if args.index_filter:
        logging.info("Loading index filter")
        index_filter = PhenoTissueGene.load_pheno_tissue_gene(args.index_filter)

        valid_genes = PhenoTissueGene.interrogate(index_filter, args.gwas_path, args.eqtl_path)
        if len(valid_genes) == 0:
            logging.info("Phenotype not in filter. Aborting")
            return
        logging.info("%d valid genes in index_filter", len(valid_genes))

    logging.info("Loading gwas")
    gwas = pandas.read_table(args.gwas_path)
    gwas = gwas.rename(columns={"A1":"effect_allele", "A2":"non_effect_allele", "b":"g_beta", "se":"g_se", "SNP":"rsid", "freq":"g_frequency"})

    logging.info("Loading eqtl frequency")
    eqtl_frequency = KeyedDataSource.load_data(args.eqtl_frequency, "variant", "frequency")

    logging.info("Loading GTEx snp")
    gtex_snp = KeyedDataSource.load_data(args.gtex_snp_path, "VariantID", "RS_ID_dbSNP142_CHG37p13", numeric=False)

    logging.info("Processing")
    e_N = int(args.eqtl_N)
    g_N = int(args.gwas_N)
    results = []

    found_variants = set()
    reporter = Utilities.PercentReporter(logging.INFO, len(eqtl_frequency), increment=1)
    reporter.update(0, "%d %% of snps from frequency found so far")

    try:

        for i,d in enumerate(DataFrameStreamer.data_frame_streamer(args.eqtl_path, "gene_id", valid_genes, ".")):
            found_variants.update(d.variant_id)
            log_str = "%d %% of snps from frequency found so far"
            if valid_genes is not None:
                log_str += " from the valid genes in the filter"
            reporter.update(len(found_variants), log_str)

            r = process(d, gwas, eqtl_frequency, gtex_snp, e_N, g_N)
            results.append(r)

    except Exception as e:
        g = None
        try:
            g = d.gene_id.values[0]
        except: pass
        logging.info("Abnormal program termination: %s\n %s", g, str(e))
        raise e

    Utilities.ensure_requisite_folders(args.output)
    results = results if len(results) else [[], [], [], [], [], []]
    results = Utilities.to_dataframe(results, ["gene_id", "P_H0", "P_H1", "P_H2", "P_H3", "P_H4"], fill_na="NA")
    compression = "gzip" if ".gz" in args.output else None
    results.to_csv(args.output, index=False, sep="\t", compression=compression)

    end = timer()
    logging.info("Successfully ran coloc in %s seconds" % (str(end - start)))


if __name__ == "__main__":
    #TODO: use N from gwas.ma if available
    import argparse

    parser = argparse.ArgumentParser("Run COLOC analysis between GWAS and GTEx eQTL")

    parser.add_argument("--gtex_snp_path", help="path to GTEx snp file")
    parser.add_argument("--eqtl_path", help="path to eqtl file")
    parser.add_argument("--eqtl_frequency", help="path to eqtl file")
    parser.add_argument("--eqtl_N", help="Number of samples in eQTL")
    parser.add_argument("--gwas_path", help="path to GWAS file")
    parser.add_argument("--gwas_N", help="Number of samples in GWAS")
    parser.add_argument("--output", help="path where COLOC result will be built")
    parser.add_argument("--index_filter", help="Optional")

    parser.add_argument("--verbosity",
                        help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything",
                        default = "10")

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)