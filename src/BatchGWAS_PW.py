#! /usr/bin/env python

import os
import logging
from gene_tools.jobs import PBS
from gene_tools.misc import PhenoTissueGene

from gene_tools import Logging



def build_command(args, gwas_file, eqtl_file):
    gwas_path = os.path.join(args.gwas_folder, gwas_file)
    gwas_key = gwas_file.split(".ma")[0]

    eqtl_path = os.path.join(args.eqtl_folder, eqtl_file)
    eqtl_key = eqtl_file.split("_Analysis")[0]

    name = "_eQTL_".join([gwas_key, eqtl_key])
    output = os.path.join(args.output_folder, name + ".txt")

    job_name = "job_gwas_pw_full_" + name

    walltime = "168:00:00" if not args.expected_walltime else args.expected_walltime
    job_mem = "4gb" if not args.expected_memory else args.expected_memory
    module_clause=\
"""module load gcc/6.2.0
module load python/2.7.13
module load gwas-pw
"""
    command = PBS.job_header(job_name, job_mem=job_mem, walltime=walltime, email=args.email,
                             module_clause=module_clause)

    exe_ = args.gwas_pw_path if args.gwas_pw_path else "gwas-pw"
    command += "python {} \\\n".format(os.path.join(args.smrtools_path, "GWAS_PW.py"))

    intermediate=os.path.join(args.working_folder, "_".join([gwas_key,eqtl_key]))
    command += "--working_folder {} \\\n".format(intermediate)

    if args.index_filter:
        command += "--index_filter {} \\\n".format(args.index_filter)
    command += "--gwas_pw_path {} \\\n".format(exe_)
    command += "--gwas_pw_bed_path {} \\\n".format(args.gwas_pw_bed_path)
    command += "--gtex_snp_path {} \\\n".format(args.gtex_snp_path)
    command += "--gwas_path {} \\\n".format(gwas_path)
    command += "--eqtl_path {} \\\n".format(eqtl_path)
    command += "--output {} \\\n".format(output)
    command += "--verbosity {}".format(args.verbosity)

    return job_name, command

def _job_gwas_pw_path(args):
    return os.path.join(args.jobs_folder, "gwas_pw")

def _job_submit_path(args):
    return os.path.join(args.jobs_folder, "submit")

def _should_skip_pheno(pheno, pheno_white_list,  index_filter):
    if pheno_white_list and not pheno in pheno_white_list:
        logging.info("%s not in whitelist, skipping", pheno)
        return True

    if index_filter is not None:
        eqtls = PhenoTissueGene.interrogate(index_filter, pheno)
        if len(eqtls) == 0:
            logging.info("%s has no significant results, skipping", pheno)
            return True

    return False

def _should_skip(pheno, eqtl, tissue_white_list, index_filter):
    if tissue_white_list and not eqtl in tissue_white_list:
        logging.info("%s not in whitelist, skipping", eqtl)
        return True

    if index_filter is not None:
        genes = PhenoTissueGene.interrogate(index_filter, pheno, eqtl)
        if len(genes) == 0:
            logging.info("(%s/%s) has no signicant results, skipping", pheno, eqtl)
            return True

    return False

def run(args):
    if not os.path.exists(args.output_folder): os.makedirs(args.output_folder)
    if not os.path.exists(args.jobs_folder): os.makedirs(args.jobs_folder)
    if not os.path.exists(args.logs_folder): os.makedirs(args.logs_folder)

    if not args.working_folder:
        logging.info("Need working folder")
        return

    gwas_files = sorted(os.listdir(args.gwas_folder))
    eqtl_files = sorted(os.listdir(args.eqtl_folder))

    pheno_white_list = set(args.pheno_white_list) if args.pheno_white_list else None
    tissue_white_list = set(args.tissue_white_list) if args.tissue_white_list else None

    index_filter = PhenoTissueGene.load_pheno_tissue_gene(args.index_filter) if args.index_filter else None

    for g in gwas_files:
        if _should_skip_pheno(g, pheno_white_list, index_filter):
            continue

        for e in eqtl_files:
            if _should_skip(g, e, tissue_white_list, index_filter):
                continue

            logging.info("Processing %s/%s", g, e)
            job_name, command = build_command(args, g, e)
            if not job_name:
                logging.info("No job for %s/%s",g,e)
                continue
            job_id = PBS.submit_command(args.jobs_folder, job_name, command,
                fake=args.fake_submission, serialize_local=args.serialize_local)
            msg = "Submitted {}|{}".format(str(job_id) if job_id else "error for %s" % (job_name), job_name)
            logging.info(msg)

    logging.info("Batched!")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Run gwas_pw analysis between GWAS and GTEx eQTL")
    parser.add_argument("--jobs_folder", help="Where the submission jobs will be saved", default="jobs")
    parser.add_argument("--logs_folder", help="Where the job logs will be saved", default="logs")
    parser.add_argument("--working_folder", help="Where the job logs will be saved", default="intermediate")
    parser.add_argument("--smrtools_path", help="Path to smr tools")
    parser.add_argument("--gwas_pw_path", help= "Path to gwas-pw tools")
    parser.add_argument("--gwas_pw_bed_path", help="path to GWAS file")
    parser.add_argument("--email", help="email for receiving notifications")
    parser.add_argument("--conda_env_name", help= "conda env name")
    parser.add_argument("--expected_walltime", help="expected job walltime")
    parser.add_argument("--expected_memory", help="expected memory")
    parser.add_argument("--index_filter", help="optional: file with filter to include in analysis")
    parser.add_argument("--gtex_snp_path", help="path to GTEx snp file")
    parser.add_argument("--eqtl_folder", help="path to eqtl file")
    parser.add_argument("--gwas_folder", help="path to GWAS file")
    parser.add_argument("--output_folder", help="path where gwas_pw result will be built")
    parser.add_argument("--verbosity", help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything", default = "10")
    parser.add_argument("--fake_submission", help="wether to submit or not", action="store_true", default=False)
    parser.add_argument("--serialize_local", help="Just run locally", action="store_true", default=False)
    parser.add_argument('--pheno_white_list', type=str, nargs='+', help='If given, will only run phenos matching this names', default=None)
    parser.add_argument('--tissue_white_list', type=str, nargs='+', help='If given, will only run phenos matching this names', default=None)

    args = parser.parse_args()

    Logging.configure_logging(int(args.verbosity))

    run(args)