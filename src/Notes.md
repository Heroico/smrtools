
# A) SMR pipeline

# 1) Reference Panel

## 1000G

1000G is used as reference panel

## Downloaded 1000G data

```bash
# Connect from a sane environment with access to internet such as syncmon
# FTP download
$ ftp ftp://ftp.1000genomes.ebi.ac.uk
ftp> cd vol1/ftp/release/20130502/
ftp> mget *
```

## Convert format

First, concatenate into one single large file with vcftools

```
# scripts/MergeVCF.sh
vcf-concat [ALL FILES HERE] | gzip -c > 1000G.phase3.vcf.gz
```

Then convert with plink

```
# scripts/VCFtoPLINK.sh
plink --vcf 1000G.phase3.vcf.gz --biallelic-only strict --maf 0.01 --vcf-filter q10 --make-bed --out results/1000G
```

## Get the 1000G snp info
This was read from IMPUTEv2 format files, holds snp definition, population frequencies, etc
`data/1000G_snp_info.txt.gz` (external tool, CStuff)


## 2) EQTL Studies

## Build BESD eQTL files from GTEx

## Get support frequencies

BESD seems to need frequencies. so. We pick them up from GTEx genotypes

```
./BatchFrequency.py
```

## Generate BESD files

```
./BatchGTExEQTLtoBESD.py
#see scripts/BatchGTExEQTLtoBESD.sh for tarbell arguments
```

# 3) GWAS

Convert GWAS to GCTA-COJO

```
./BatchGWASToSMR.py
# see scripts/BatchGWASToSMRTarbell.sh for tarbell submission
```

# 4) SMR

```
./BatchSMR.py
#see scripts/BatchSMRSubmissionTarbell.sh for tarbell submission
```

# SMR for All Associations
There is also a variant that operates in all associations GTEx eQTL files.
The logic is somewhat different.
You will still need to build the reference panel data and convert GWAS files to COJO as in the ordinary pipeline.
It requires a file listing triads of phenotype, tissue, genes to filter the data.
Otherwise it would be just too large to fit into a reasonable amount of memory

Then, you use:
```
./BatchSMROnGTEx.py
#or a proper wrapper
```


# B) COLOC

There is a fast tool for significant eQTL results files. It reuses some data from the SMR pipeline. It needs:
* GWAS as in the SMR pipeline
* eqtls as output by `BatchGTExEQTLConversion.py`
* `BatchColoc.py` or a proper wrapper.


# B2) COLOC FULL
There is an alternative toolset that runs on GTEx full eQTL (all associations).
There is also a variant that operates on full eQTL results. It needs:
* GWAS in SMR results
* all association eQTL from GTEx
* optionally a file to filter significant phenotypes, tissue, gene triads.

# C) eCAVIAR

## The first step is to preprocess the eQTL and GWAS files into the raw information we need

```
# the tool
eCAVIARPreprocessGTExeQTL.py
# the batcher of the tool
BatcheCAVIARPreprocessGTExeQTL.py
# the wrapper
```

Then you can use the actual analysis

```
# The tool
eCAVIARProcess.py
# the batcher
BatcheCAVIARProcess.py
#Wrapper with tarbell environment
BatcheCAVIARProcessTarbell.sh
```

#Conda evironment (for example, for use in RCC)

```
#bash
conda create --name numa python=2 #or whatever
source activate numa

conda install nomkl numpy scipy pandas rpy2

#or if using older version
#conda install anaconda-client
#conda install ipython
#conda install nomkl numpy scipy pandas
#conda install --channel https://conda.anaconda.org/r rpy2

#R
install.packages("coloc")
```

# Pending

VCF to plink: use `--snps-only` to remove non snps in VCF? (i.e rs143435517)
SMR Step: filter individuals/population in 1000G reference, to match GWAS population?

COLOC.py/ColocFull.py: use n from gwas.ma if available