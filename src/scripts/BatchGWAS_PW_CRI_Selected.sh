#!/bin/bash

export SMR_TOOLS=/group/im-lab/nas40t2/abarbeira/software/smrtools/src/

python $SMR_TOOLS/BatchGWAS_PW.py \
--smrtools_path $SMR_TOOLS \
--gwas_pw_bed_path /group/im-lab/nas40t2/abarbeira/data/ldetect-data/EUR/fourier_ls-all.bed \
--index_filter /group/im-lab/nas40t2/abarbeira/data/smr_root/GWAS/metaxcan_prototype_significant.txt \
--email mundoconspam@gmail.com \
--expected_walltime 72:00:00 \
--gwas_folder /group/im-lab/nas40t2/abarbeira/projects/smr/results/gwas \
--gtex_snp_path /group/im-lab/nas40t2/abarbeira/data/smr_root/GTEx/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz \
--eqtl_folder /group/im-lab/nas40t2/Data/dbGaP/GTEx/V6p-gtexportal/GTEx_Analysis_v6p_all-associations \
--output_folder results/gwas_pw_selected \
--verbosity  10

# --pheno_white_list GIANT_HEIGHT.ma pgc.scz2.ma GLGC_Mc_LDL.ma \