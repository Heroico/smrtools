#!/bin/bash

export SMR_TOOLS=/group/im-lab/nas40t2/abarbeira/software/smrtools/src/

python $SMR_TOOLS/BatchCOLOCFull.py \
--smrtools_path  $SMR_TOOLS \
--email mundoconspam@gmail.com \
--expected_walltime 8:00:00 \
--index_filter /scratch/abarbeira3/data/GWAS/metaxcan_prototype_significant.txt \
--conda_env_name numa \
--gwas_folder /scratch/abarbeira3/data/GWAS/smr/ \
--gwas_data_file /scratch/abarbeira3/data/GWAS/pheno_metadata_internal.tsv \
--gtex_snp_path /scratch/abarbeira3/data/GTEx/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz \
--eqtl_folder /scratch/abarbeira3/data/GTEx/v6p/GTEx_Analysis_v6p_all-associations \
--eqtl_frequency_folder /scratch/abarbeira3/data/GTEx/v6p/freq_full \
--eqtl_samples_file /scratch/abarbeira3/data/GTEx/v6p/GTEx_v6p_samples.txt \
--output_folder results/coloc_metaxcansignificant \
--tissue_white_list Adipose_Subcutaneous_Analysis.v6p.all_snpgene_pairs.txt.gz \
--fake_submission \
--verbosity  10

#---pheno_white_list GIANT_HEIGHT.ma pgc.scz2.ma RA_OKADA_TRANS_ETHNIC.ma \
#--fake_submission \