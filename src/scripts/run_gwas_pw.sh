#!/usr/bin/env bash


/home/numa/Documents/Projects/3rd/smrtools/src/GWAS_PW.py \
--index_filter /home/numa/Documents/Projects/data/Production/metaxcan_prototype_significant.txt \
--gwas_pw_path /home/numa/Documents/Projects/3rd/gwas-pw/src/gwas-pw \
--gwas_pw_bed_path /home/numa/Documents/Projects/3rd/ldetect-data/EUR/fourier_ls-all.bed \
--working_folder intermediate/gwas_pw \
--gtex_snp_path /home/numa/Documents/Projects/data/GTEx/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz \
--eqtl_path /home/numa/Documents/Projects/data/GTEx/v6p/eqtl_all/Adipose_Subcutaneous_Analysis.v6p.all_snpgene_pairs.txt.gz \
--gwas_path /home/numa/Documents/Projects/data/smr/results/gwas/GIANT_HEIGHT.ma \
--verbosity 7 \
--output results/test/gwas-pw/GIANT_HEIGHT_eQTL_Adipose_Subcutaneous_f.txt

#--index_filter /home/numa/Documents/Projects/data/Production/metaxcan_prototype_significant.txt \