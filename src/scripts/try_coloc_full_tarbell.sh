#!/bin/bash
#PBS -N kk_coloc
#PBS -S /bin/bash
#PBS -l walltime=8:00:00
#PBS -l nodes=1:ppn=1
#PBS -l mem=4gb
#PBS -o ${PBS_JOBNAME}.o${PBS_JOBID}.log
#PBS -e ${PBS_JOBNAME}.e${PBS_JOBID}.err

module load R/3.3.1
source activate 2_12

/group/im-lab/nas40t2/abarbeira/software/smrtools/src/ColocFull.py \
--gtex_snp_path /scratch/abarbeira3/data/GTEx/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz \
--eqtl_path /group/im-lab/nas40t2/Data/dbGaP/GTEx/V6p-gtexportal/GTEx_Analysis_v6p_all-associations/Adipose_Subcutaneous_Analysis.v6p.all_snpgene_pairs.txt.gz \
--eqtl_frequency /scratch/abarbeira3/data/GTEx/v6p/freq_full/Adipose_Subcutaneous.txt.gz \
--eqtl_N 298 \
--gwas_path /scratch/abarbeira3/data/GWAS/smr/GIANT_HEIGHT.ma \
--gwas_N 253288 \
--output results/coloc/GIANT_HEIGHT_eQTL_Adipose_Subcutaneous.txt.gz > kk.txt