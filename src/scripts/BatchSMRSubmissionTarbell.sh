#!/bin/bash

export SMR_TOOLS=/group/im-lab/nas40t2/abarbeira/software/smrtools/src

python $SMR_TOOLS/BatchSMR.py \
--smr_command /group/im-lab/nas40t2/abarbeira/software/smr/smr_Linux \
--smrtools_path $SMR_TOOLS \
--cojo_folder /scratch/abarbeira3/data/GWAS/smr \
--reference /scratch/abarbeira3/data/P_1000G/1000G \
--besd_folder /scratch/abarbeira3/data/GTEx/BESD \
--results_path results/smr