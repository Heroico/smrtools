#!/usr/bin/env bash

export PBS_O_WORKDIR=.
export SMR_TOOLS=/home/numa/Documents/Projects/3rd/smrtools/src

python $SMR_TOOLS/BatchCOLOC.py \
--smrtools_path  $SMR_TOOLS \
--gwas_folder "/home/numa/Documents/Projects/data/smr/GWAS/" \
--gwas_data_file "/home/numa/Documents/Projects/data/Production/pheno_metadata_internal.tsv" \
--eqtl_folder "/home/numa/Documents/Projects/data/GTEx/v6p/synth/eqtl" \
--eqtl_samples_file "/home/numa/Documents/Projects/data/GTEx/v6p/synth/GTEx_samples.txt" \
--output_folder "results/coloc" \
--serialize_local \
--verbosity  10

#--serialize_local \
#--fake_submission \
