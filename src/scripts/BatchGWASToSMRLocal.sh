#!/bin/bash

export PBS_O_WORKDIR=.
export SMR_TOOLS=/home/numa/Documents/Projects/3rd/smrtools/src

python $SMR_TOOLS/BatchGWASToSMR.py \
--smrtools_path  $SMR_TOOLS \
--gwas_folder /home/numa/Documents/Projects/data/Production_2 \
--tg_snp_info  /home/numa/Documents/Projects/data/1000G_synth/1000G_snp_info.txt.gz \
--results_path results/gwas \
--fake_submission \
--verbosity  10

#--fake_submission \
#--gwas_folder /home/numa/Documents/Projects/data/Production/ \