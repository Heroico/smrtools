#! /usr/bin/env python
import gzip

VCF="1000G.phase3.vcf.gz"

chroms = set()
with gzip.open(VCF) as file:
    for i,line in enumerate(file):
        if "#" in line: continue
        comps = line.strip().split()
        chroms.add(comps[0])

print chroms
