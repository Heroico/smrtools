#!/bin/bash

export PBS_O_WORKDIR=.
export SMR_TOOLS=/home/numa/Documents/Projects/3rd/smrtools/src

python $SMR_TOOLS/BatchGWAS_PW.py \
--smrtools_path $SMR_TOOLS \
--gwas_pw_path  /home/numa/Documents/Projects/3rd/gwas-pw/src/gwas-pw \
--email mundoconspam@gmail.com \
--expected_walltime 3:00:00 \
--expected_memory 4gb \
--index_filter /home/numa/Documents/Projects/data/Production/metaxcan_prototype_significant.txt \
--gtex_snp_path /home/numa/Documents/Projects/data/GTEx/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz \
--gwas_folder /home/numa/Documents/Projects/data/smr/results/gwas/ \
--eqtl_folder /home/numa/Documents/Projects/data/GTEx/v6p/eqtl_all/ \
--output_folder results/gwas_pw \
--fake_submission \
--verbosity  10

#--pheno_white_list GIANT_HEIGHT.ma pgc.scz2.ma GLGC_Mc_LDL.ma \
#--index_filter /home/numa/Documents/Projects/data/Production/metaxcan_prototype_significant.txt \
#--pheno_white_list GIANT_HEIGHT.ma \