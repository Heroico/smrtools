#!/bin/bash

export PBS_O_WORKDIR=.
export SMR_TOOLS=/home/numa/Documents/Projects/3rd/smrtools/src

python $SMR_TOOLS/BatchCOLOCFull.py \
--smrtools_path  $SMR_TOOLS \
--email mundoconspam@gmail.com \
--expected_walltime 2:00:00 \
--expected_memory 24gb \
--index_filter /home/numa/Documents/Projects/data/Production/metaxcan_prototype_significant.txt \
--gtex_snp_path /home/numa/Documents/Projects/data/GTEx/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz \
--gwas_folder /home/numa/Documents/Projects/data/smr/results/gwas/ \
--gwas_data_file /home/numa/Documents/Projects/data/Production/pheno_metadata_internal.tsv \
--eqtl_folder /home/numa/Documents/Projects/data/GTEx/v6p/eqtl_all/ \
--eqtl_frequency_folder /home/numa/Documents/Projects/data/GTEx/v6p/synth/freq_full \
--eqtl_samples_file /home/numa/Documents/Projects/data/GTEx/v6p/synth/GTEx_v6p_samples.txt \
--output_folder results/coloc \
--fake_submission \
--verbosity  10

#--pheno_white_list GIANT_HEIGHT.ma pgc.scz2.ma GLGC_Mc_LDL.ma \
#--index_filter /home/numa/Documents/Projects/data/Production/metaxcan_prototype_significant.txt \
#--pheno_white_list GIANT_HEIGHT.ma \