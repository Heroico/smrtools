#!/bin/bash

./CheckBatch.py \
--jobs_folder jobs_e \
--logs_folder logs_e \
--results_path results/ecaviar/last_step \
--job_pattern 'job_ecaviar_(.*).sh' \
--result_pattern '.txt.gz$' \
--output out.log \
--log_file kk.log
