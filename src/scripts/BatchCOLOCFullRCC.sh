#!/bin/bash

export SMR_TOOLS=/project/haky/im-lab/nas40t2/abarbeira/software/smrtools/src

python $SMR_TOOLS/BatchCOLOCFullRCC.py \
--smrtools_path  $SMR_TOOLS \
--conda_env_name numa \
--gtex_snp_path /project/haky/Data/GTEx/V6p/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz \
--gwas_folder /project/haky/Data/GWAS/synth \
--gwas_data_file /project/haky/Data/GWAS/pheno_metadata_internal.tsv \
--eqtl_folder  /project/haky/Data/GTEx/V6p/GTEx_Analysis_v6p_all_associations \
--eqtl_frequency_folder /project/haky/Data/GTEx/V6p/synth/freq_full \
--eqtl_samples_file /project/haky/Data/GTEx/V6p/synth/GTEx_v6p_samples.txt \
--output_folder results/coloc \
--pheno_white_list GIANT_HEIGHT.ma pgc.scz2.ma RA_OKADA_TRANS_ETHNIC.ma \
--submission_size 8 \
--verbosity  10