#! /usr/bin/env python

import os
import gzip


#from ..gene_tools import Logging

def run(args):
    folder = args.folder
    paths = [os.path.join(folder, x) for x in sorted(os.listdir(folder))]

    snps=set()
    column = int(args.column)
    for p in paths:
        print "Processing %s" % p
        with gzip.open(p) as file:
            for i, line in enumerate(file):
                if i==0: continue
                comps = line.strip().split()
                snp = comps[column]
                if ";" in snp:
                    print "kk", snp
                    exit(0)

    print "Saving"
    with gzip.open(args.output, "w") as file:
        for snp in snps:
            file.write("%s\n" % snp)

    print "ran"

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Convert GWAS to eCAVIAR preprocess")

    parser.add_argument("--folder", help="Gwas input file path")
    parser.add_argument("--column", help="Gwas input file path")
    parser.add_argument("--output", help="result file")

    args = parser.parse_args()

    run(args)