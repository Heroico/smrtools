#!/bin/bash

export SMR_TOOLS=/group/im-lab/nas40t2/abarbeira/software/smrtools/src

python $SMR_TOOLS/BatchSMROnGTEx.py \
--conda_env_name numa \
--working_folder working \
--email mundoconspam@gmail.com \
--expected_walltime 8:00:00 \
--smrtools_path  $SMR_TOOLS \
--index_filter /scratch/abarbeira3/data/GWAS/metaxcan_prototype_significant.txt \
--reference /scratch/abarbeira3/data/P_1000G/1000G \
--smr_command /group/im-lab/nas40t2/abarbeira/software/smr/smr_Linux \
--gwas_folder /scratch/abarbeira3/data/GWAS/smr/ \
--gtex_eqtl_folder /scratch/abarbeira3/data/GTEx/v6p/GTEx_Analysis_v6p_all-associations \
--gtex_frequency_folder /scratch/abarbeira3/data/GTEx/v6p/freq_full \
--gtex_snp_path /scratch/abarbeira3/data/GTEx/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz \
--gencode_path /scratch/abarbeira3/data/GTEx/v6p/gencode.v19.genes.v6p_model.patched_contigs.gtf.gz \
--results_path results/smr_metaxcansignificant

#--fake_submission \
#--tissue_whitelist Adipose_Subcutaneous_Analysis.v6p.all_snpgene_pairs.txt.gz \