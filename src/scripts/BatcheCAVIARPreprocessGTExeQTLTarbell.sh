#!/bin/bash


export SMR_TOOLS=/group/im-lab/nas40t2/abarbeira/software/smrtools/src/
export TOOL=$SMR_TOOLS/eCAVIARPreprocessGTExeQTL.py

python $SMR_TOOLS/BatcheCAVIARPreprocessGTExeQTL.py \
--tool_path \"$TOOL\" \
--gtex_eqtl_folder /scratch/abarbeira3/data/GTEx/v6p/eqtl/ \
--gtex_snp_path /scratch/abarbeira3/data/GTEx/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz \
--output_folder results/ecaviar/pre/eqtl