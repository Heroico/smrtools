#!/bin/bash

export SMR_TOOLS=/group/im-lab/nas40t2/project/t2d/software/smrtools/src/

python $SMR_TOOLS/BatchCOLOCFull.py \
--smrtools_path  $SMR_TOOLS \
--email mundoconspam@gmail.com \
--conda_env_name numa \
--gwas_folder /group/im-lab/nas40t2/project/t2d/runs/gwas_conversion/results/gwas/ \
--gwas_data_file /group/im-lab/nas40t2/project/t2d/data/GWAS/pheno_metadata_internal.tsv \
--gtex_snp_path /group/im-lab/nas40t2/project/t2d/data/GTEx/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz \
--eqtl_folder /group/im-lab/nas40t2/project/t2d/data/GTEx/GTEx_Analysis_v6p_all-associations \
--eqtl_frequency_folder /group/im-lab/nas40t2/project/t2d/data/GTEx/synth/v6p_freq_full \
--eqtl_samples_file /group/im-lab/nas40t2/project/t2d/data/GTEx/GTEx_v6p_samples.txt \
--output_folder results/coloc_all \
--fake_submission \
--verbosity  10
