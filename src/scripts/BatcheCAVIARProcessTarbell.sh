#!/usr/bin/env bash

export SMR_TOOLS=/group/im-lab/nas40t2/abarbeira/software/smrtools/src

python $SMR_TOOLS/BatcheCAVIARProcess.py \
--jobs_folder jobs_e \
--logs_folder logs_e \
--working_folder _working \
--output_folder results/ecaviar/last_step \
--script_path  $SMR_TOOLS/eCAVIARProcess.py \
--ecaviar_path /group/im-lab/nas40t2/abarbeira/software/ecaviar/caviar/CAVIAR-C++/eCAVIAR \
--eqtl_folder /scratch/abarbeira3/data/ecaviar/eqtl \
--eqtl_reference_folder /scratch/abarbeira3/data/GTEx/v6p/geno \
--gwas_folder /scratch/abarbeira3/data/ecaviar/gwas \
--gwas_reference_folder /group/im-lab/nas40t2/abarbeira/data/dosages/TGF_EUR_CAVIAR \
--gwas_reference_pattern "1000GP_Phase3_chr([0-9]+).dosage.gz" \
--verbosity 9