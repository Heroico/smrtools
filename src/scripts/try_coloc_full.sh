#!/bin/bash

/home/numa/Documents/Projects/3rd/smrtools/src/ColocFull.py \
--index_filter /home/numa/Documents/Projects/data/Production/metaxcan_prototype_significant.txt \
--gtex_snp_path /home/numa/Documents/Projects/data/GTEx/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz \
--eqtl_path /home/numa/Documents/Projects/data/GTEx/v6p/eqtl_all/Adipose_Subcutaneous_Analysis.v6p.all_snpgene_pairs.txt.gz \
--eqtl_frequency /home/numa/Documents/Projects/data/GTEx/v6p/synth/freq_full/Adipose_Subcutaneous.txt.gz \
--eqtl_N 298 \
--gwas_path /home/numa/Documents/Projects/data/smr/GWAS/GIANT_HEIGHT.ma \
--gwas_N 253288 \
--output results/test/coloc/GIANT_HEIGHT_eQTL_Adipose_Subcutaneous.txt  > kk_c.txt

#/home/heroico/Documents/Projects/3rd/smrtools/src/ColocFull.py \
#--gtex_snp_path /home/heroico/Documents/data/GTEx/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz \
#--eqtl_path /home/heroico/Documents/data/GTEx/Adipose_Subcutaneous_Analysis.v6p.all_snpgene_pairs.txt.gz \
#--eqtl_frequency /home/heroico/Documents/data/GTEx/synth/freq_full/Adipose_Subcutaneous.txt.gz \
#--eqtl_N 298 \
#--gwas_path /home/heroico/Documents/data/GWAS/synth/GIANT_HEIGHT.ma \
#--gwas_N 253288 \
#--output results/test/coloc/GIANT_HEIGHT_eQTL_Adipose_Subcutaneous.txt  > kk_c.txt

#--eqtl_path /home/heroico/Documents/data/GTEx/adipose_t.txt \