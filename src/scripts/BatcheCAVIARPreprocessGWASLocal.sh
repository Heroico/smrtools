#!/bin/bash

export PBS_O_WORKDIR=.
export SMR_TOOLS=/home/heroico/Documents/Projects/3rd/smrtools/src

python $SMR_TOOLS/BatcheCAVIARPreprocessGWAS.py \
--tool_path $SMR_TOOLS/eCAVIARPreprocessGWAS.py \
--gwas_folder "data/Production/" \
--results_path "results/ecaviar/pre/gwas" \
--fake_submission \
--verbosity  10

#--serialize_local \