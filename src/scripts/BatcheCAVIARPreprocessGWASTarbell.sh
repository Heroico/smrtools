#!/bin/bash

export SMR_TOOLS=/group/im-lab/nas40t2/abarbeira/software/smrtools/src

python $SMR_TOOLS/BatcheCAVIARPreprocessGWAS.py \
--tool_path  $SMR_TOOLS/eCAVIARPreprocessGWAS.py \
--gwas_folder "/group/im-lab/nas40t2/Data/SummaryResults/Production" \
--results_path "results/ecaviar/pre/gwas" \
--verbosity  10