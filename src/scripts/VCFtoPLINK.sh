#!/bin/bash

#PBS -N job_Plink_Merge
#PBS -S /bin/bash
#PBS -l walltime=24:00:00
#PBS -l nodes=1:ppn=1
#PBS -l mem=64gb
#PBS -o logs/${PBS_JOBNAME}.o${PBS_JOBID}.log
#PBS -e logs/${PBS_JOBNAME}.e${PBS_JOBID}.log

cd $PBS_O_WORKDIR

module load plink/1.90

rm -rf logs
mkdir logs
plink \
--memory 65536 \
--merge-list all_files.txt \
--make-bed --out 1000G