#!/bin/bash
#PBS -N kk_job
#PBS -S /bin/bash
#PBS -l walltime=8:00:00
#PBS -l nodes=1:ppn=1
#PBS -l mem=8gb
#PBS -o ${PBS_JOBNAME}.o${PBS_JOBID}.log
#PBS -e ${PBS_JOBNAME}.e${PBS_JOBID}.err

cd $PBS_O_WORKDIR

module load python/2.7.9

python /group/im-lab/nas40t2/abarbeira/software/smrtools/src/eCAVIARProcess.py \
--eqtl_path /scratch/abarbeira3/data/ecaviar/eqtl_2/Adipose_Subcutaneous.txt.gz \
--gwas_path /scratch/abarbeira3/data/ecaviar/gwas/DIAGRAM_T2D_TRANS_ETHNIC.txt.gz \
--gwas_reference_folder /group/im-lab/nas40t2/abarbeira/data/dosages/TGF_EUR_CAVIAR \
--eqtl_reference_file /scratch/abarbeira3/data/GTEx/v6p/geno/Adipose_Subcutaneous_Analysis.snps.txt.gz \
--intermediate _intermediate \
--ecaviar_path /group/im-lab/nas40t2/abarbeira/software/ecaviar/caviar/CAVIAR-C++/eCAVIAR \
--verbosity 9 \
--output results/test.txt > kk.txt