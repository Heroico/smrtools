#!/usr/bin/env bash

export SMR_TOOLS=/group/im-lab/nas40t2/project/t2d/software/smrtools/src

python $SMR_TOOLS/BatchGWASToSMR.py \
--smrtools_path  $SMR_TOOLS \
--gwas_folder /group/im-lab/nas40t2/project/t2d/data/GWAS/ \
--tg_snp_info  /group/im-lab/nas40t2/project/t2d/data/1000G_synth/1000G_snp_info.txt.gz \
--results_path results/gwas \
--verbosity  10