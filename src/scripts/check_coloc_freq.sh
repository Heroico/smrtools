#!/bin/bash

./CheckBatch.py \
--jobs_folder jobs_gtexf \
--logs_folder logs_gtexf \
--results_path results/eqtl/freq_full/ \
--job_pattern 'job_eqtlf_(.*).sh' \
--result_pattern '.txt.gz$' \
--output out.log \
--log_file kk.log