#!/bin/bash

export PBS_O_WORKDIR=.
export SMR_TOOLS=/home/numa/Documents/Projects/3rd/smrtools/src

python $SMR_TOOLS/BatchSMROnGTEx.py \
--smrtools_path  $SMR_TOOLS \
--index_filter /home/numa/Documents/Projects/data/Production/metaxcan_prototype_significant.txt \
--reference /home/numa/Documents/Projects/data/1000G_synth/P_1000G/1000G \
--smr_command smr_Linux \
--working_folder working \
--gwas_folder /home/numa/Documents/Projects/data/smr/results/gwas \
--gtex_eqtl_folder /home/numa/Documents/Projects/data/GTEx/v6p/eqtl_all/ \
--gtex_frequency_folder /home/numa/Documents/Projects/data/GTEx/v6p/synth/freq_full \
--gtex_snp_path /home/numa/Documents/Projects/data/GTEx/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz \
--gencode_path /home/numa/Documents/Projects/data/GTEx/v6p/gencode.v19.genes.v6p_model.patched_contigs.gtf.gz \
--results_path results/smr \
--tissue_white_list Adipose_Subcutaneous_Analysis.v6p.all_snpgene_pairs.txt.gz \
--pheno_white_list GIANT_HEIGHT.ma \
--smr_log_folder log_smr \
--fake_submission
