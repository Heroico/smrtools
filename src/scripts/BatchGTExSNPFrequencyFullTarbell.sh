#!/bin/bash

export SMR_TOOLS=/group/im-lab/nas40t2/abarbeira/software/smrtools/src

python $SMR_TOOLS/BatchGTExSNPFrequencyFull.py \
--script_path $SMR_TOOLS/GTExSNPFrequencyFull.py \
--jobs_folder jobs_gtexf \
--logs_folder logs_gtexf \
--gtex_geno_folder  /scratch/abarbeira3/data/GTEx/v6p/geno \
--output_folder results/eqtl/freq_full