#! /bin/bash


smr_Linux \
 --bfile data/1000G_plink/1000G \
 --gwas-summary results/diagram.ma \
 --beqtl-summary data/BESDeQTL/Whole_Blood_Analysis/Whole_Blood_Analysis \
 --out diagram