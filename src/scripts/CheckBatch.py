#! /usr/bin/env python
"""
Quick & Ugly script to check job submission status, copy pasted for self completeness
"""
import logging
import os
import sys
import time
import re
from subprocess import Popen, PIPE, call
import pandas

def configure_logging(level=5, target=sys.stderr, log_file=None):
    logger = logging.getLogger()
    logger.setLevel(level)

    # create console handler and set level to info
    handler = logging.StreamHandler(target)
    handler.setLevel(level)
    formatter = logging.Formatter("%(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    if log_file:
        fileHandler = logging.FileHandler("log_file")
        fileHandler.setFormatter(formatter)
        logger.addHandler(fileHandler)

def submit_command(path, serialize_local):
    if serialize_local:
        logging.info("Running %s", path)
        call(["bash", path])
        return

    retry = 0

    command = ["qsub",path]
    def submit():
        logging.info("Submitting Command: %s", " ".join(command))
        proc = Popen(command,stdout=PIPE, stderr=PIPE)
        out, err = proc.communicate()
        exitcode = proc.returncode
        return exitcode, out, err

    exitcode, out, err = submit()
    while exitcode != 0 and retry < 10:
        logging.info("retry %s %i-th attempt", path, retry)
        exitcode, out, err = submit()
        retry += 1
        time.sleep(0.1)
    time.sleep(0.1)
    return out.strip() if exitcode==0 else None

def get_job_targets(path, jobs_pattern):
    #'job_smr_(.*).sh'
    jobs_regexp = re.compile(jobs_pattern)
    jobs = sorted(os.listdir(path))
    job_names = [split[1] for split in [os.path.split(x) for x in jobs]]
    job_names = [jobs_regexp.search(x).group(1) for x in job_names]
    jobs = [os.path.join(path, x) for x in jobs]
    return job_names, jobs

def get_result_present(results_path, targets, results_pattern):
    #".smr$"
    logging.info("Found %d expected results", len(targets))
    res = re.compile(results_pattern)
    results = os.listdir(results_path)
    results = {res.split(x)[0] for x in results}
    logging.info("Found %d results", len(results))
    present = [(x in results) for x in targets]
    logging.info("Present: %d", len(present))
    return present

def build_presence(job_path, results_path, jobs_pattern, results_pattern):
    targets, jobs = get_job_targets(job_path, jobs_pattern)
    present = get_result_present(results_path, targets, results_pattern)
    presence = {"target":targets, "present":present, "job":jobs}
    presence = pandas.DataFrame(presence)
    presence = presence.sort_values(by="target")
    presence = presence[["target", "present", "job"]]
    return presence

def resubmit(presence, serialize_local):
    for row in presence.values:
        if row[1] == True: continue
        path = row[2]
        submit_command(path, serialize_local)


def run(args):
    presence = build_presence(args.jobs_folder, args.results_path, args.job_pattern, args.result_pattern)

    presence.to_csv(args.output, index=False, sep="\t")

    if args.resubmit:
        resubmit(presence,args.serialize_local)

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Compare jobs to expected result")
    parser.add_argument("--jobs_folder", help="Where the submission jobs will be saved", default="jobs")
    parser.add_argument("--logs_folder", help="Where the job logs will be saved", default="logs")
    parser.add_argument("--results_path", help= "Path to results files", default="results/coloc")
    parser.add_argument("--job_pattern", help="regexp to extract target job. Ecample 'job_smr_(.*).sh'")
    parser.add_argument("--result_pattern", help="regexp to extract result name. Example '.txt$'")
    parser.add_argument("--output", help="Path where the output will be saved", default="job_log.txt")
    parser.add_argument("--verbosity", help="Log verbosity level. 1 is everything being logged. 10 is only high level messages, above 10 will hardly log anything", default = "10")
    parser.add_argument("--resubmit", help="wether to submit missing jobs", action="store_true", default=False)
    parser.add_argument("--serialize_local", help="If resubmitting job, just run locally", action="store_true", default=False)
    parser.add_argument("--log_file", help="On top of everything else, save to a file")

    args = parser.parse_args()

    configure_logging(int(args.verbosity), log_file=args.log_file)

    run(args)