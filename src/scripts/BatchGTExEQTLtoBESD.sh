#!/bin/bash

export SMR_TOOLS=/group/im-lab/nas40t2/abarbeira/software/smrtools/src

python $SMR_TOOLS/BatchGTExEQTLtoBESD.py \
--smrtools_path \"$SMR_TOOLS\" \
--smr_command  /group/im-lab/nas40t2/abarbeira/software/smr/smr_Linux \
--gtex_eqtl_folder  /scratch/abarbeira3/data/GTEx/v6p/eqtl \
--gtex_frequency_folder  /scratch/abarbeira3/data/GTEx/v6p/freq \
--gtex_snp_path /scratch/abarbeira3/data/GTEx/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz \
--gencode_path /scratch/abarbeira3/data/GTEx/v6p/gencode.v19.genes.v6p_model.patched_contigs.gtf.gz \
--working_path working \
--results_path results