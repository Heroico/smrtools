#!/usr/bin/env bash

#!/bin/bash

#PBS -N job_VCF_Merge
#PBS -S /bin/bash
#PBS -l walltime=2:00:00
#PBS -l nodes=1:ppn=1
#PBS -l mem=4gb
#PBS -o logs/${PBS_JOBNAME}.o${PBS_JOBID}.log
#PBS -e logs/${PBS_JOBNAME}.e${PBS_JOBID}.log

module load vcftools/0.1.12b

vcf-concat -f vcf_files.txt | gzip -c > 1000G.phase3.vcf.gz