#!/bin/bash

./CheckBatch.py \
--jobs_folder jobs \
--logs_folder logs \
--results_path results/coloc_all \
--job_pattern 'job_coloc_full_(.*).sh' \
--result_pattern '.txt$' \
--output out.log \
--log_file kk.log

#--resubmit
