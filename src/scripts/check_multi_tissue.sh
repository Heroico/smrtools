#!/bin/bash

./CheckBatch.py \
--jobs_folder jobs \
--logs_folder logs \
--results_path results/multi_tissue_v1.5 \
--job_pattern 'job_(.*)_cer_0.0001.sh' \
--result_pattern '.txt$' \
--output out.log \
--log_file kk.log

#--resubmit