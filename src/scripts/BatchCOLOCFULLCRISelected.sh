#!/bin/bash

export SMR_TOOLS=/group/im-lab/nas40t2/abarbeira/software/smrtools/src/

python $SMR_TOOLS/BatchCOLOCFull.py \
--smrtools_path  $SMR_TOOLS \
--email mundoconspam@gmail.com \
--expected_walltime 72:00:00 \
--conda_env_name numa \
--gwas_folder /group/im-lab/nas40t2/abarbeira/projects/smr/results/gwas \
--gwas_data_file /group/im-lab/nas40t2/abarbeira/data/smr_root/GWAS/pheno_metadata_internal.tsv \
--gtex_snp_path /group/im-lab/nas40t2/abarbeira/data/smr_root/GTEx/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz \
--eqtl_folder /group/im-lab/nas40t2/Data/dbGaP/GTEx/V6p-gtexportal/GTEx_Analysis_v6p_all-associations \
--eqtl_frequency_folder /group/im-lab/nas40t2/abarbeira/data/smr_root/GTEx/v6p/freq_full \
--eqtl_samples_file /group/im-lab/nas40t2/abarbeira/data/smr_root/GTEx/v6p/GTEx_v6p_samples.txt \
--output_folder results/coloc_selected \
--pheno_white_list GIANT_HEIGHT.ma pgc.scz2.ma GLGC_Mc_LDL.ma \
--verbosity  10