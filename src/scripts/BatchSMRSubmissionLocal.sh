#!/bin/bash

export SMR_TOOLS=/home/numa/Documents/Projects/3rd/smrtools/src

python $SMR_TOOLS/BatchSMR.py \
--smr_command smr_Linux \
--smrtools_path $SMR_TOOLS \
--cojo_folder results/gwas/ \
--reference /home/numa/Documents/Projects/data/1000G_synth/plink/1000G \
--besd_folder /home/numa/Documents/Projects/data/GTEx/v6p/synth/BSDeQTL \
--results_path results/smr \
--fake_submission