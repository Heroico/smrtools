#!/usr/bin/env bash

/home/numa/Documents/Projects/3rd/smrtools/src/SMROnGTEx.py \
--smr_command smr_Linux \
--reference /home/numa/Documents/Projects/data/1000G_synth/P_1000G/1000G \
--gwas_path /home/numa/Documents/Projects/data/smr/GWAS/GIANT_HEIGHT.ma \
--index_filter /home/numa/Documents/Projects/data/Production/metaxcan_prototype_significant.txt \
--gtex_snp_path /home/numa/Documents/Projects/data/GTEx/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz \
--gtex_eqtl_path /home/numa/Documents/Projects/data/GTEx/v6p/eqtl_all/Adipose_Subcutaneous_Analysis.v6p.all_snpgene_pairs.txt.gz \
--gtex_frequency_path /home/numa/Documents/Projects/data/GTEx/v6p/synth/freq_full/Adipose_Subcutaneous.txt.gz \
--gencode_path /home/numa/Documents/Projects/data/GTEx/v6p/gencode.v19.genes.v6p_model.patched_contigs.gtf.gz \
--working_folder working_kk \
--output_prefix results/test/smr/kk  > kk.txt