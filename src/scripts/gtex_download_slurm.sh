#!/bin/bash
#SBATCH --job-name=download_gtex
#SBATCH --output=logs/download_gtex.log
#SBATCH --error=logs/download_.err
#SBATCH --partition=sandyb
#SBATCH --mem-per-cpu=4096
#SBATCH --ntasks=1
#SBATCH --time=60:00:00


wget http://www.gtexportal.org/static/datasets/gtex_analysis_v6p/single_tissue_eqtl_data/GTEx_Analysis_v6p_all-associations.tar
#wget http://www.gtexportal.org/static/datasets/gtex_analysis_v6p/single_tissue_eqtl_data/GTEx_Analysis_v6p_eQTL.tar
#wget http://www.gtexportal.org/static/datasets/gtex_analysis_v6p/single_tissue_eqtl_data/GTEx_Analysis_v6p_eQTL_expression_matrices.tar
#wget http://www.gtexportal.org/static/datasets/gtex_analysis_v6p/single_tissue_eqtl_data/GTEx_Analysis_v6p_eQTL_covariates.tar.gz
