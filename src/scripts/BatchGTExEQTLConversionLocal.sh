#!/usr/bin/env bash

export PBS_O_WORKDIR="."
export SMR_TOOLS=/home/numa/Documents/Projects/3rd/smrtools/src

python $SMR_TOOLS/BatchGTExEQTLConversion.py \
--smrtools_path \"$SMR_TOOLS\" \
--gtex_eqtl_folder data/GTEx_Analysis_v6p_eQTL \
--gtex_frequency_folder data/gtex_frequency \
--gtex_snp_path data/GTEx_Analysis_v6_OMNI_genot_1KG_imputed_var_chr1to22_info4_maf01_CR95_CHR_POSb37_ID_REF_ALT.txt.gz \
--serialize_local \
--output_folder results/eqtl